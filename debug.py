import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid, gridplot
from bokeh.models import ColumnDataSource, TabPanel, Tabs, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.live_map import LiveMap
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial
from MAVSDKManager import MAVSDKManager
import time
import shortuuid


doc = curdoc()

global_settings = GlobalSettings()
mavsdk_manager = MAVSDKManager()
#astro = mavsdk_manager.get_or_create_client(14601)
boarai = mavsdk_manager.get_or_create_client(14602)

plan_mission_and_upload_button = Button(label="Plan & Upload Mission", height=80, sizing_mode="stretch_width", align="end", button_type="success")

arm_astro_button = Button(label="Arm Astro", height=80, sizing_mode="stretch_width", align="end", button_type="danger")
arm_boarai_button = Button(label="Arm boarAI", height=80, sizing_mode="stretch_width", align="end", button_type="danger")

disarm_astro_button = Button(label="Disarm Astro", height=80, sizing_mode="stretch_width", align="end", button_type="success")
disarm_boarai_button = Button(label="Disarm boarAI", height=80, sizing_mode="stretch_width", align="end", button_type="success")


arm_both_button = Button(label="Arm ALL", height=80, sizing_mode="stretch_width", align="end", button_type="danger")


start_astro_mission_button = Button(label="Start Astro Mission", height=80, sizing_mode="stretch_width", align="end", button_type="default")
start_boarai_mission_button = Button(label="Start boarAI Mission", height=80, sizing_mode="stretch_width", align="end", button_type="default")
start_both_mission_button = Button(label="Start ALL Missions", height=80, sizing_mode="stretch_width", align="end", button_type="default")

request_all_params_button = Button(label="Request all params", height=80, sizing_mode="stretch_width", align="end", button_type="default")


async def arm_astro():
    pass
    #await astro.action.arm()

async def arm_boarai():
    await boarai.action.arm()

def arm_astro_button_clicked():
    asyncio.ensure_future(arm_astro())

def arm_boarai_button_clicked():
    asyncio.ensure_future(arm_boarai())

def arm_both_button_clicked():
    asyncio.ensure_future(arm_boarai())
    asyncio.ensure_future(arm_astro())

async def disarm_astro():
    pass
    #await astro.action.disarm()

async def disarm_boarai():
    await boarai.action.disarm()

def disarm_astro_button_clicked():
    asyncio.ensure_future(disarm_astro())

def disarm_boarai_button_clicked():
    asyncio.ensure_future(disarm_boarai())

async def start_astro_mission():
    pass
    #await astro.mission.start_mission()

async def start_boarai_mission():
    await boarai.mission.start_mission()

def start_astro_mission_button_clicked():
    asyncio.ensure_future(start_astro_mission())

def start_boarai_mission_button_clicked():
    asyncio.ensure_future(start_boarai_mission())

def start_both_mission_button_clicked():
    asyncio.ensure_future(start_boarai_mission())
    asyncio.ensure_future(start_astro_mission())

async def request_all_params():
    pass

def request_all_params_button_clicked():
    asyncio.ensure_future(request_all_params())


arm_astro_button.on_click(arm_astro_button_clicked)
arm_boarai_button.on_click(arm_boarai_button_clicked)

disarm_astro_button.on_click(disarm_astro_button_clicked)
disarm_boarai_button.on_click(disarm_boarai_button_clicked)

arm_both_button.on_click(arm_both_button_clicked)


start_astro_mission_button.on_click(start_astro_mission_button_clicked)
start_boarai_mission_button.on_click(start_boarai_mission_button_clicked)
start_both_mission_button.on_click(start_both_mission_button_clicked)

request_all_params_button.on_click(request_all_params_button_clicked)

async def async_test_attitude():
    await asyncio.sleep(4)
    async for battery in boarai.telemetry.attitude_quaternion():
        print(str(battery.timestamp_us))

async def mavsdk_connect():
    #await astro.connect()
    print("connecting...")
    await boarai.connect()
    print("done")


def btn_enable_arm_clicked():
    global_settings.can_arm = True

btn_enable_arm = Button(label="ENABLE ARMING FOR ALL", height=80, sizing_mode="stretch_width", align="end", button_type="danger")
btn_enable_arm.on_click(btn_enable_arm_clicked)

asyncio.ensure_future(mavsdk_connect())

asyncio.ensure_future(async_test_attitude())

doc.add_root(column(plan_mission_and_upload_button, arm_astro_button, arm_boarai_button,arm_both_button, disarm_astro_button, disarm_boarai_button, btn_enable_arm, start_astro_mission_button, start_boarai_mission_button, start_both_mission_button, request_all_params_button))

