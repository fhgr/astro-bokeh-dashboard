import os
import glob
import json
from datetime import datetime
from datetime import timedelta
from typing import Tuple

class MissionManagerV2:
    
    def delete_mission(self, mission):
        id = mission if isinstance(mission, str) else mission["id"]
        os.remove(f'./missionsV2/{id}.json')

    def save_mission(self, mission):
        id = mission["id"]
        json_string = json.dumps(mission, indent=4, default=str)
        f = open(f'./missionsV2/{id}.json', "w")
        f.write(json_string)
        f.close()

    def create_mission(self, name: str, id: str, vehicle_ids):
        mission = {
            "id": id,
            "name": name,
            "completed": False,
            "created_at": datetime.now().strftime("%Y-%m-%d %H.%M"),
            "start_time": None,
            "end_time": None,
            "vehicle_ids": vehicle_ids
        }
        self.save_mission(mission)

    def get_mission_by_id(self, id):
        mission = [m for m in self.get_all_missions() if m["id"] == id]
        return mission[0]

    def get_all_missions(self):
        files = glob.glob("./missionsV2/*.json")
        missions = list()
        for file in files:
            f = open(file)       
            dict = json.load(f)
            missions.append(dict)
            f.close()
        return missions

    def set_mission_completed(self, mission, start_time: datetime, end_time: datetime):
        mission["completed"] = True
        mission["start_time"] = start_time
        mission["end_time"] = end_time
        self.save_mission(mission)

    def get_planned_missions(self):
        planned_missions =  [m for m in self.get_all_missions() if m["completed"] == False]
        return planned_missions
    
    def get_completed_missions(self):
        completed_missions =  [m for m in self.get_all_missions() if m["completed"] == True]
        return completed_missions

    def get_latest_mission_id(self):
        completed_missions = self.get_completed_missions()
        latest_mission_time = datetime.fromisoformat("2022-06-20 14:42:07.608360")
        latest_mission_id = None
        for m in completed_missions:
            if datetime.fromisoformat(m["end_time"]) > latest_mission_time:
                latest_mission_time = datetime.fromisoformat(m["end_time"])
                latest_mission_id = m["id"]
        print(f"newest mission is: {latest_mission_id}")
        return latest_mission_id


class MissionManager:

    def __init__(self):
        self.__mission_start_time = None
        self.__mission_end_time = None

    def save_mission(self, start, end):
        name = start.strftime("%Y-%m-%d %H.%M")
        mission = {
            "name": name,
            "start": start,
            "end": end
        }
        
        json_string = json.dumps(mission, indent=4, default=str)
        f = open(f'./missions/{name}.json', "w")
        f.write(json_string)
        f.close()
        return name

    def get_missions(self):
        files = glob.glob("./missions/*.json")
        names = [os.path.splitext(os.path.basename(x))[0] for x in files]
        print(names)
        return names
    
    def get_latest_mission(self):
        missions = self.get_missions()
        missions.sort(reverse=True)
        return missions[0]
    
    def get_mission(self, name):
        file = f"./missions/{name}.json"
        f = open(file)       
        dict = json.load(f)
        f.close()
        return dict

    def start_mission(self):
        print("Mission started!")
        self.__mission_start_time = datetime.now()

    def stop_mission(self) -> Tuple[datetime, datetime]:
        print("Mission stopped!")
        self.__mission_end_time = datetime.now()
        self.__mission_start_time = self.__mission_start_time + timedelta(seconds=5)
        self.__mission_end_time = self.__mission_end_time + timedelta(seconds=5)
        self.save_mission(self.__mission_start_time, self.__mission_end_time)
        return self.__mission_start_time, self.__mission_end_time