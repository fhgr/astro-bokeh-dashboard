import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid
from bokeh.models import ColumnDataSource, TabPanel, CustomJS, Tabs, MultiChoice, Paragraph, Label, CheckboxButtonGroup, TextInput, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.plan_map import PlanMap 
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial
import time
import shortuuid
from VehicleManager import VehicleManager

doc = curdoc()

vehicle_manager = VehicleManager()
vehicles = vehicle_manager.get_all_vehicles()

textbox_name = TextInput(title="Name:", sizing_mode="scale_width")
textbox_id = TextInput(title="ID:", sizing_mode="scale_width")
textbox_mavsdk_port = TextInput(title="MAVSDK Port:", sizing_mode="scale_width")

discard_button = Button(label="Discard", button_type="danger")
ev = CustomJS(args={}, code='setTimeout(() => {window.location.href = "list_vehicles";}, 500);')
discard_button.js_on_click(ev)

save_button = Button(label="Save", button_type="success")

def save_clicked():
    if len(textbox_name.value) > 1 and len(textbox_id.value) > 1 and len(textbox_mavsdk_port.value) > 1:
        vehicle_manager.create_vehicle(textbox_name.value, textbox_id.value, textbox_mavsdk_port.value)
        print("SAVE")

save_button.on_click(save_clicked)
save_button.js_on_click(ev)

col1 = column(textbox_name, textbox_id, textbox_mavsdk_port, row(discard_button, save_button))

doc.add_root(column(col1, sizing_mode="scale_both", styles=dict(padding="10px")))
