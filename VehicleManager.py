import os
import glob
import json
from datetime import datetime
from datetime import timedelta
from typing import Tuple

class VehicleManager:
    
    def __init__(self):
        self.my_property = "123"

    def delete_vehicle(self, vehicle):
        id = vehicle["id"]
        os.remove(f'./vehicles/{id}.json')

    def save_vehicle(self, vehicle):
        id = vehicle["id"]
        json_string = json.dumps(vehicle, indent=4, default=str)
        f = open(f'./vehicles/{id}.json', "w")
        f.write(json_string)
        f.close()

    def create_vehicle(self, name: str, vehicle_id: str, mavsdk_port: str):
        mission = {
            "name": name,
            "id": vehicle_id,
            "mavsdk_port": mavsdk_port,
        }
        self.save_vehicle(mission)

    def get_vehicle_by_id(self, id):
        vehicles = [m for m in self.get_all_vehicles() if m["id"] == id]
        return vehicles[0]

    def get_all_vehicles(self):
        files = glob.glob("./vehicles/*.json")
        vehicles = list()
        for file in files:
            f = open(file)       
            dict = json.load(f)
            vehicles.append(dict)
            f.close()
        return vehicles
    
    def get_vehicles_by_ids(self, ids):
        vehicles = self.get_all_vehicles()
        filtered_vehicles = list()
        for vehicle in vehicles:
            if vehicle["id"] in ids:
                filtered_vehicles.append(vehicle)
        return filtered_vehicles
    
    def get_all_vehicle_ids(self):
        return [x["id"] for x in self.get_all_vehicles()]