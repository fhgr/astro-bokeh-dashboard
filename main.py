import datetime
import numpy as np

from bokeh.layouts import column, row, grid
from bokeh.models import ColumnDataSource, TabPanel, Tabs, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 

from bokeh.layouts import layout
import time
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance

from Plots import *

from influxdb_client import InfluxDBClient, Point
import asyncio
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManager
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial

from mqtt.provider import Provider

doc = curdoc()

settings = GlobalSettings()
settings.set_chemical_vehicle_id("14551")
settings.set_radiation_vehicle_id("14552")
settings.set_influx_address("http://192.168.1.24:8086")
settings.set_mqtt_address("192.168.1.24")
settings.set_output_backend("canvas") #canvas, webgl

session = SessionSettings()
session.set_is_main(settings.get_is_main())


is_dev_mode = True

# singleton - only 1 instance per server
mqtt_provider = MQTTDataProvider()
mqtt_is_connected = mqtt_provider.connect() if not is_dev_mode else False

# transient - 1 instance per thread (tab / user)
influx_is_connected = False
if not is_dev_mode:
    try:
        influx_client = InfluxDBClient(url=settings.get_influx_address(), token=settings.get_influx_token(), org="fhgr")
        influx_is_connected = True
    except:
        pass

print(f"mqtt_is_connected: {mqtt_is_connected}")
print(f"influx_is_connected: {influx_is_connected}")

# transient - 1 instance per thread (tab / user)
mission_manager = MissionManager()

# transient - 1 instance per thread (tab / user)
waypoint_manager = WaypointManager()

# used for formatting line plots
datetime_tick_formatter = DatetimeTickFormatter(microseconds = "%H:%M:%S", milliseconds = "%H:%M:%S", seconds = "%H:%M:%S", minutes = "%H:%M:%S", hours = "%H:%M:%S", days="%H:%M:%S",months="%H:%M:%S", years="%H:%M:%S",  minsec = "%H:%M:%S" )

# update interval in ms for EVERY plot
periodic_update_interval_ms = 100

is_live = True

# vehicle ids are discovered by listening on all messages via MQTT. vehicle_id = udp source port


vehicle_ids = ["14551", "14552"] # mqtt_provider.get_discovered_vehicles() if mqtt_provider.isRunning() else mqtt_provider.connect_and_discover_devices() 
# discovery of 1 sec
time.sleep(1)
vehicle_ids = mqtt_provider.get_connected_vehicles()
if len(vehicle_ids) <= 0:
    print("NO VEHICLES DETECTED! Using default debug data as fallback")
    vehicle_ids = ["14551", "14552"]
    mqtt_provider.set_debug_data()
    #vehicle_ids = mqtt_provider.get_connected_vehicles()
else:
    print(f"Detected vehicles: {vehicle_ids}")

# UI elements
custom_map = Map(vehicle_ids)
attitude = Attitude(vehicle_ids)
distance_sensor = DistanceSensor(vehicle_ids, attitude.figure.x_range)
battery = Battery(vehicle_ids, attitude.figure.x_range)
chemical = Chemical(vehicle_ids, attitude.figure.x_range)
radiation = Radiation(vehicle_ids, attitude.figure.x_range)
custom_map2 = Map(vehicle_ids)


plots = [custom_map, attitude, distance_sensor, battery, chemical, radiation, custom_map2]

def reset_plots():
    for p in plots:
        p.reset()


# periodic callback defined by 'update_interval_ms' 
@count()
def update(c):

    # stream live data
    if settings.get_is_live():
        for vehicle_id in vehicle_ids:
            for p in plots:
                p.stream(mqtt_provider, c, str(vehicle_id))
    
    # someone else started mission, start too
    if not session.get_is_mission_running() and settings.get_is_mission_running():
        start_mission()

    if session.get_is_mission_running() and not settings.get_is_mission_running():
        stop_mission()  


measurement_modes = list(map(str, vehicle_ids))
measurement_selection = CheckboxGroup(labels = measurement_modes, active=[0,1])

chemical_button = Toggle(label=f"{'Hide' if custom_map.get_chemical_visibility() else 'Show'} chemical data", active = custom_map.get_chemical_visibility(), button_type="primary")
radiation_button = Toggle(label=f"{'Hide' if custom_map.get_radiation_visibility() else 'Show'} radiation data", active = custom_map.get_radiation_visibility(), button_type="warning")

def chemical_button_click(state):
    if state == True:
        chemical_button.label = "Hide chemical data"
    else:
        chemical_button.label = "Show chemical data"
    custom_map.set_chemical_visibility(state)

def radiation_button_click(state):
    if state == True:
        radiation_button.label = "Hide radiation data"
    else:
        radiation_button.label = "Show radiation data"
    custom_map.set_radiation_visibility(state)

chemical_button.on_click(chemical_button_click)
radiation_button.on_click(radiation_button_click)

div = Div(sizing_mode="stretch_both")
select_measurement_mode_header = Div(text="<h3>Select drones</h3>")

select_target_area_header = Div(text="<h3>Select target area</h3>")

select_chemical_area = Button(label="Select chemical area", button_type="primary")

def select_chemical_area_click():
    custom_map.activate_draw()
    data = custom_map.get_drawn_area()
    waypoints = waypoint_manager.build_waypoints("2", data["x"][0], data["y"][0])
    if not waypoints:
        print("Not drawing path")
        return
    custom_map.clear_waypoints()
    custom_map.set_waypoints(waypoints)

select_chemical_area.on_click(select_chemical_area_click)

select_radiation_area = Button(label="Select radiation area", button_type="warning")
def select_radiation_area_click():
    pass

def measurement_mode_changed(attr, old, new):
    select_chemical_area.visible = 0 in new
    select_radiation_area.visible = 1 in new

measurement_selection.on_change("active", measurement_mode_changed)

start_mission_button = Button(label="Start Mission", disabled=session.get_is_readonly(), height=80, sizing_mode="stretch_width", align="end", button_type="success")
stop_mission_button = Button(label="Stop Mission", height=80, sizing_mode="stretch_width", align="end", button_type="danger")
configuration_panel_right = column(select_measurement_mode_header, measurement_selection,  select_target_area_header, select_chemical_area, select_radiation_area, div, start_mission_button, width=300, sizing_mode="stretch_height", margin=(20,40,10,40))


info_panel_right = column(chemical_button, radiation_button, div, stop_mission_button, visible=False, width=300, sizing_mode="stretch_height", margin=(20,40,10,40))
spacer = column(Div(), width=300, margin=(20,40,10,40))
panel_right = column(spacer, configuration_panel_right, info_panel_right, sizing_mode="stretch_height")

planning_and_live_data_tab = TabPanel(child=column(row(column(custom_map.figure, sizing_mode="stretch_both"), panel_right, sizing_mode="stretch_both"), sizing_mode="stretch_both"), title="Planning & Live Data")

def load_mission(start, end):
    settings.set_is_live(False)
    for p in plots:
        p.reset()
        for v in vehicle_ids:
            p.set(influx_client, start, end, v)

def refresh_missions_selection(select_mission):
    missions = mission_manager.get_missions()
    missions.sort(reverse=True)
    select_mission.options = missions
    select_mission.value = missions[0] if len(missions) > 0 else ""

select_mission = Select(title="Select mission to display:")
refresh_missions_selection(select_mission)

def button_load_mission_clicked():
    mission = mission_manager.get_mission(select_mission.value)
    start = datetime.datetime.strptime(mission["start"], '%Y-%m-%d %H:%M:%S.%f')
    end =  datetime.datetime.strptime(mission["end"], '%Y-%m-%d %H:%M:%S.%f')
    load_mission(start,end)

button_load_mission = Button(label="Load Mission")
button_load_mission.on_click(button_load_mission_clicked)

mission_detail_tab = TabPanel(child=column(row(column(select_mission), column(button_load_mission)),row(custom_map2.figure, min_height=600, sizing_mode="stretch_both"), row(attitude.figure, distance_sensor.figure, min_height=600, sizing_mode="stretch_both"), row(battery.figure, chemical.figure, min_height=600, sizing_mode="stretch_both"), row(radiation.figure, min_height=600, sizing_mode="stretch_both"), sizing_mode="stretch_both"), title="Mission Detail")

astro = System(mavsdk_server_address='192.168.1.24', port=14601)
boarai = System(mavsdk_server_address='192.168.1.24', port=14602)

plan_mission_and_upload_button = Button(label="Plan & Upload Mission", height=80, sizing_mode="stretch_width", align="end", button_type="success")

arm_astro_button = Button(label="Arm Astro", height=80, sizing_mode="stretch_width", align="end", button_type="danger")
arm_boarai_button = Button(label="Arm boarAI", height=80, sizing_mode="stretch_width", align="end", button_type="danger")

disarm_astro_button = Button(label="Disarm Astro", height=80, sizing_mode="stretch_width", align="end", button_type="success")
disarm_boarai_button = Button(label="Disarm boarAI", height=80, sizing_mode="stretch_width", align="end", button_type="success")


arm_both_button = Button(label="Arm ALL", height=80, sizing_mode="stretch_width", align="end", button_type="danger")


start_astro_mission_button = Button(label="Start Astro Mission", height=80, sizing_mode="stretch_width", align="end", button_type="default")
start_boarai_mission_button = Button(label="Start boarAI Mission", height=80, sizing_mode="stretch_width", align="end", button_type="default")
start_both_mission_button = Button(label="Start ALL Missions", height=80, sizing_mode="stretch_width", align="end", button_type="default")

request_all_params_button = Button(label="Request all params", height=80, sizing_mode="stretch_width", align="end", button_type="default")

async def plan_mission_and_upload():
    takeoff = mission.MissionItem(latitude_deg=46.856236881607714, longitude_deg=9.516925224483572, relative_altitude_m=50, speed_m_s=15, is_fly_through=False, gimbal_pitch_deg=0, gimbal_yaw_deg=0, camera_action=mission.MissionItem.CameraAction.NONE, loiter_time_s=0, camera_photo_interval_s=0, acceptance_radius_m=5, yaw_deg=0, camera_photo_distance_m=0)
    waypoint1 = mission.MissionItem(latitude_deg=46.85637578, longitude_deg=9.51717454, relative_altitude_m=50, speed_m_s=15, is_fly_through=False, gimbal_pitch_deg=0, gimbal_yaw_deg=0, camera_action=mission.MissionItem.CameraAction.NONE, loiter_time_s=0, camera_photo_interval_s=0, acceptance_radius_m=5, yaw_deg=0, camera_photo_distance_m=0)
    waypoint2 = mission.MissionItem(latitude_deg=46.85676845, longitude_deg=9.51686265, relative_altitude_m=50, speed_m_s=15, is_fly_through=False, gimbal_pitch_deg=0, gimbal_yaw_deg=0, camera_action=mission.MissionItem.CameraAction.NONE, loiter_time_s=0, camera_photo_interval_s=0, acceptance_radius_m=5, yaw_deg=0, camera_photo_distance_m=0)
    waypoint3 = mission.MissionItem(latitude_deg=46.85639871, longitude_deg=9.51617912, relative_altitude_m=50, speed_m_s=15, is_fly_through=False, gimbal_pitch_deg=0, gimbal_yaw_deg=0, camera_action=mission.MissionItem.CameraAction.NONE, loiter_time_s=0, camera_photo_interval_s=0, acceptance_radius_m=5, yaw_deg=0, camera_photo_distance_m=0)
    landing = mission.MissionItem(latitude_deg=46.856236881607714, longitude_deg=9.516925224483572, relative_altitude_m=50, speed_m_s=15, is_fly_through=False, gimbal_pitch_deg=0, gimbal_yaw_deg=0, camera_action=mission.MissionItem.CameraAction.NONE, loiter_time_s=0, camera_photo_interval_s=0, acceptance_radius_m=5, yaw_deg=0, camera_photo_distance_m=0)
    mission_items = [takeoff, waypoint1,waypoint2,waypoint3, landing]
    mission_plan = mission.MissionPlan(mission_items=mission_items)
    await astro.mission.upload_mission(mission_plan=mission_plan)

def plan_mission_and_upload_button_clicked():
    asyncio.ensure_future(plan_mission_and_upload())

async def arm_astro():
    await astro.action.arm()

async def arm_boarai():
    await boarai.action.arm()

def arm_astro_button_clicked():
    asyncio.ensure_future(arm_astro())

def arm_boarai_button_clicked():
    asyncio.ensure_future(arm_boarai())

def arm_both_button_clicked():
    asyncio.ensure_future(arm_boarai())
    asyncio.ensure_future(arm_astro())

async def disarm_astro():
    await astro.action.disarm()

async def disarm_boarai():
    await boarai.action.disarm()

def disarm_astro_button_clicked():
    asyncio.ensure_future(disarm_astro())

def disarm_boarai_button_clicked():
    asyncio.ensure_future(disarm_boarai())


async def start_astro_mission():
    await astro.mission.start_mission()

async def start_boarai_mission():
    await boarai.mission.start_mission()



def start_astro_mission_button_clicked():
    asyncio.ensure_future(start_astro_mission())

def start_boarai_mission_button_clicked():
    asyncio.ensure_future(start_boarai_mission())

def start_both_mission_button_clicked():
    asyncio.ensure_future(start_boarai_mission())
    asyncio.ensure_future(start_astro_mission())

async def request_all_params():
    test = await astro.info.get_identification()
    print("VERSION:")
    print(test)
    #await drone.telemetry.battery()
    #await drone.param.get_all_params()

def request_all_params_button_clicked():
    asyncio.ensure_future(request_all_params())

plan_mission_and_upload_button.on_click(plan_mission_and_upload_button_clicked)

arm_astro_button.on_click(arm_astro_button_clicked)
arm_boarai_button.on_click(arm_boarai_button_clicked)

disarm_astro_button.on_click(disarm_astro_button_clicked)
disarm_boarai_button.on_click(disarm_boarai_button_clicked)

arm_both_button.on_click(arm_both_button_clicked)


start_astro_mission_button.on_click(start_astro_mission_button_clicked)
start_boarai_mission_button.on_click(start_boarai_mission_button_clicked)
start_both_mission_button.on_click(start_both_mission_button_clicked)

request_all_params_button.on_click(request_all_params_button_clicked)

testing_tab = TabPanel(child=column(plan_mission_and_upload_button, arm_astro_button, arm_boarai_button,arm_both_button, disarm_astro_button, disarm_boarai_button, start_astro_mission_button, start_boarai_mission_button, start_both_mission_button, request_all_params_button), title="Testing")

async def start_mission_async():
    pass
    #await drone.action.arm()
    #await drone.mission.start_mission()
    
def update_text(text):
    stop_mission_button.label = text

async def async_test_attitude():
    async for attitude in astro.telemetry.attitude_euler():
        doc.add_next_tick_callback(partial(update_text, str(attitude.pitch_deg)))



def start_mission():
    # asyncio.ensure_future(start_mission_async())
    reset_plots()
    session.set_is_mission_running(True)

    # adjust UI accordingly
    configuration_panel_right.visible = False
    info_panel_right.visible = True

# button event, also affects global settings
def start_mission_button_clicked():
    settings.set_is_mission_running(True)
    mission_manager.start_mission()
    start_mission()

def stop_mission():
    session.set_is_live(False)
    session.set_is_mission_running(False)
    configuration_panel_right.visible = True
    info_panel_right.visible = False

    # fix time mismatch notebook
    refresh_missions_selection(select_mission)
    button_load_mission_clicked()

# mission ended, stop live data and display historical data
def stop_mission_button_clicked():
    settings.set_is_live(False)
    settings.set_is_mission_running(False)
    mission_manager.stop_mission()
    stop_mission()


start_mission_button.on_click(start_mission_button_clicked)
stop_mission_button.on_click(stop_mission_button_clicked)

async def connect_mavsdk_clients():
    global is_dev_mode
    #if is_dev_mode:
    #    return
    #await astro.connect()
    await boarai.connect()
    print("MAVSDK CONNECTED")

asyncio.ensure_future(connect_mavsdk_clients())
tabs = Tabs(tabs = [planning_and_live_data_tab, mission_detail_tab, testing_tab], sizing_mode="stretch_both")
doc.add_root(tabs)
doc.add_periodic_callback(update, periodic_update_interval_ms)
