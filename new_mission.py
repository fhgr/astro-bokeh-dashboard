import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid
from bokeh.models import ColumnDataSource, TabPanel, CustomJS, Tabs, MultiChoice, Paragraph, Label, CheckboxButtonGroup, TextInput, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.plan_map import PlanMap 
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
from VehicleManager import VehicleManager
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial
import time
import shortuuid

doc = curdoc()

mission_manager = MissionManagerV2()
mission_manager.get_completed_missions()
mission_manager.get_planned_missions()

vehicle_manager = VehicleManager()


txt1 = TextInput(title="Name:", sizing_mode="scale_width")
txt2 = TextInput(title="ID:", value=shortuuid.uuid(), disabled=True, sizing_mode="scale_width")

vehicles = vehicle_manager.get_all_vehicle_ids()
#btngroup = CheckboxButtonGroup(labels = vehicles, height_policy="fixed",  height=txt1.height)
btngroup = MultiChoice(options=vehicles, value=vehicles, title="Vehicles", sizing_mode="stretch_width")
plan_map = PlanMap(vehicles)
col1 = column(txt1, txt2)

discard_button = Button(label="Discard", button_type="danger")
ev = CustomJS(args={}, code='setTimeout(() => {window.location.href = "missions";}, 500);')
discard_button.js_on_click(ev)

save_button = Button(label="Save", button_type="success", disabled=True)

def save_clicked():
    mission_manager.create_mission(txt1.value, txt2.value, btngroup.value)
    print("SAVE")
    #discard_button.trigger("js_click", 'old', 'new')

save_button.on_click(save_clicked)
save_button.js_on_click(ev)

col3 = row(discard_button, save_button)
draw_col = column(Button(label="Draw Radiation Area"), Button(label="Draw Chemical Area"))
my_grid = grid([[col1, btngroup, draw_col, col3], [None, None, None,None], [plan_map.figure]], sizing_mode="scale_both")

#elements = column(my_grid, btngroup, plan_map.figure, align="center", sizing_mode="scale_both", styles=dict(padding="10px"))
elements = column(my_grid, sizing_mode="scale_both", styles=dict(padding="10px"))
doc.add_root(elements)


def on_tick():
    if txt1.value is not None and len(txt1.value) > 2:
        save_button.disabled = False
    else:
        save_button.disabled = True

doc.add_periodic_callback(on_tick, 100)
