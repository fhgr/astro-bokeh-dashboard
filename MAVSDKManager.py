import os
import glob
import json
from datetime import datetime
from datetime import timedelta
from typing import Tuple
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from GlobalSettings import GlobalSettings

class MAVSDKManager:

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(MAVSDKManager, cls).__new__(cls)
            cls.instance.__initialized = False
        return cls.instance

    def __init__(self):
        if self.__initialized:
            return
        self.__initialized = True
        self.connections = {}

    def get_or_create_client(self, port):
        global_settings = GlobalSettings()
        connection = self.connections.get(port)
        if connection is None:
            print(f"creating new connection on port {port}")
            connection = self.connections[port] = System(mavsdk_server_address=global_settings.get_server_ip(), port=port)
        else:
            print(f"using existing connection on port {port}")
        return connection
