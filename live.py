import datetime
import socket
from bokeh.layouts import column, row, gridplot
from bokeh.models import Switch, CustomJS, Button, Div
from bokeh.driving import count
from bokeh.plotting import curdoc
import math
from MAVSDKManager import MAVSDKManager
from MQTTMessageProvider import MQTTDataProvider
from Plots.live_map import LiveMap
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
import asyncio
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from functools import partial
from VehicleManager import VehicleManager
import time
import shortuuid

doc = curdoc()

mission_manager = MissionManagerV2()
vehicle_manager = VehicleManager()
mavsdk_manager = MAVSDKManager()

arguments = doc.session_context.request.arguments
mission_id = arguments["id"][0].decode("utf-8") if len(arguments) > 0 else None
is_mission = mission_id is not None
mission = None if not is_mission else mission_manager.get_mission_by_id(mission_id)

global_settings = GlobalSettings()
session_settings = SessionSettings()
session_settings.set_is_main(global_settings.get_is_main())

is_dev_mode = False

# singleton - only 1 instance per server
mqtt_provider = MQTTDataProvider()
mqtt_is_connected = mqtt_provider.connect() if not is_dev_mode else False

if is_dev_mode:
    mqtt_provider.set_debug_data()

vehicles = vehicle_manager.get_all_vehicles() if not is_mission else vehicle_manager.get_vehicles_by_ids(mission["vehicle_ids"])
vehicle_ids = vehicle_manager.get_all_vehicle_ids() if not is_mission else mission["vehicle_ids"]

mqtt_provider.set_debug_data(vehicle_ids)
print(f"Using vehicles: {vehicle_ids}")

mavsdks_by_vehicle_id = {}

for vehicle in vehicles:
    if int(vehicle["mavsdk_port"]) <= 0:
        print(f"no mavsdk configured for vehicle {vehicle['id']}. skipping...")
        continue
    mavsdks_by_vehicle_id[vehicle["id"]] = mavsdk_manager.get_or_create_client(int(vehicle["mavsdk_port"]))

# UI elements
attitude = Attitude(vehicle_ids)
distance_sensor = DistanceSensor(vehicle_ids, attitude.figure.x_range)
battery = Battery(vehicle_ids, attitude.figure.x_range)
chemical = Chemical(vehicle_ids, attitude.figure.x_range)
radiation = Radiation(vehicle_ids, attitude.figure.x_range)

# wait for some MQTT messages to arrive (GPS)
time.sleep(1)

gps_message = mqtt_provider.get_message("GLOBAL_POSITION_INT", vehicle_ids[0])
default_lon, default_lat = 9.517440, 46.856504

if gps_message is not None and gps_message["Lon"] > 0:
    default_lat = gps_message["Lat"] / 10000000.0
    default_lon = gps_message["Lon"] / 10000000.0
custom_map2 = LiveMap(vehicle_ids, default_lat, default_lon)

plots = [attitude, distance_sensor, battery, chemical, radiation, custom_map2]

btn1 = Button(label="Map Fullscreen: Off", height=50, width=140, button_type="primary")
btn2 = Button(label="Hide Radiation Data", height=50, width=140, button_type="default")
btn3 = Button(label="Hide Chemical Data", height=50, width=140, button_type="default")
button_start_recording = Button(label="Start Recording", height=50, width=140, button_type="success", disabled=global_settings.get_is_recording())
btn5 = Button(label="Hide 14551", height=50, width=140, button_type="default")
btn6 = Button(label="Hide 14552", height=50, width=140, button_type="default")


def toggle_changed(attr, old, new, id, switch):
    for p in plots:
        if not switch.active:
            if hasattr(p, 'hide_vehicle'):
                p.hide_vehicle(id)
        else:
            if hasattr(p, 'show_vehicle'):
                p.show_vehicle(id)        

switches = list()
# toggle data switches
for id in vehicle_ids:
    vehicle = vehicle_manager.get_vehicle_by_id(id)
    label = Div(text=f'{vehicle["name"]} ({vehicle["id"]})')
    switch = Switch(active=True)
    switches.append(label)
    switches.append(switch)
    switch.on_change("active", partial(toggle_changed, id=id, switch=switch))

btn_launch_mission = Button(label="Launch Mission", height=50, width=140, button_type="success", visible=is_mission, disabled=(not is_mission or not global_settings.can_arm))

successful_launched_ids = list()

async def launch_mission_async(id):
    if id in successful_launched_ids:
        print(f" {id} already launched. skipping")
        return
    client = mavsdks_by_vehicle_id[id]
    try:
        print(f"launching for {id}")
        await client.action.arm()
        await asyncio.sleep(1)
        await client.mission.start_mission()
        successful_launched_ids.append(id)
    except Exception as e:
        print(f"MAVSDK Error: {e}")

def btn_launch_mission_clicked():
    for id in mavsdks_by_vehicle_id:
        asyncio.ensure_future(launch_mission_async(id))

btn_launch_mission.on_click(btn_launch_mission_clicked)

btn_col = column([btn1, btn_launch_mission, button_start_recording, column(switches)])
map_row = row(custom_map2.figure, btn_col, sizing_mode="scale_both")
my_grid = gridplot([[attitude.figure, distance_sensor.figure, battery.figure], [chemical.figure, radiation.figure]], toolbar_location='right',toolbar_options=dict(logo=None), sizing_mode="stretch_both")

def toggle_map_fullscreen_button_clicked():
    my_grid.visible = not my_grid.visible
    btn1.label = "Map Fullscreen: On" if not my_grid.visible else "Map Fullscreen: Off"

def toggle_radiation_data_clicked():
    custom_map2.set_radiation_visibility(not custom_map2.get_radiation_visibility())
    btn2.label = "Hide Radiation Data" if custom_map2.get_radiation_visibility() else "Show Radiation Data"

def toggle_chemical_data_clicked():
    custom_map2.set_chemical_visibility(not custom_map2.get_chemical_visibility())
    btn3.label = "Hide Chemical Data" if custom_map2.get_chemical_visibility() else "Show Chemical Data"

def toggle_14551_clicked():
    custom_map2.adjust_zoom()
    btn3.label = "Hide 14551 Data" if custom_map2.get_chemical_visibility() else "Show 14551 Data"

def toggle_14552_clicked():
    btn3.label = "Hide 14552 Data" if custom_map2.get_chemical_visibility() else "Show 14552 Data"

start_time = datetime.datetime.now()

def start_stop_recording_button_clicked():
    global start_time
    global mission_id

    if mission_id is None:
        mission_id = shortuuid.uuid()
        mission_manager.create_mission("AutoGenerated", mission_id, vehicle_ids)

    if not global_settings.get_is_recording():
        session_settings.set_is_recording(True)
        global_settings.set_is_recording(True)
        start_time = datetime.datetime.now()
        button_start_recording.label = "Stop Recording"
        button_start_recording.button_type = "danger"
    else:
        session_settings.set_is_recording(False)
        global_settings.set_is_recording(False)
        button_start_recording.label = "Start Recording"
        button_start_recording.button_type = "success"
        mission = mission_manager.get_mission_by_id(mission_id)
        mission_manager.set_mission_completed(mission, start_time, datetime.datetime.now())
        button_start_recording.name = "view_mission"

btn1.on_click(toggle_map_fullscreen_button_clicked)
btn2.on_click(toggle_radiation_data_clicked)
btn3.on_click(toggle_chemical_data_clicked)
button_start_recording.on_click(start_stop_recording_button_clicked)
btn5.on_click(toggle_14551_clicked)
btn6.on_click(toggle_14552_clicked)

# periodic callback defined by 'update_interval_ms' 
@count()
def update_plots(c):
    for vehicle_id in vehicle_ids:
        for p in plots:
            p.stream(mqtt_provider, c, str(vehicle_id))

button_start_recording.js_on_change("name", CustomJS(args={}, code='window.location.href = "view_mission"'))

def update_ui():

    if global_settings.can_arm and len(successful_launched_ids) == len(mavsdks_by_vehicle_id):
        print("all vehicles launched, deactivate armability and button")
        global_settings.can_arm = False

    if global_settings.can_arm and btn_launch_mission.disabled:
        print("enabling launch button")
        btn_launch_mission.disabled = False

    if not global_settings.can_arm and not btn_launch_mission.disabled:
        print("disabling launch button")
        btn_launch_mission.disabled = True

    if len(successful_launched_ids) == len(mavsdks_by_vehicle_id) and not btn_launch_mission.disabled:
        print("disabling launch button")
        btn_launch_mission.disabled = True

    if not session_settings.get_is_recording() and global_settings.get_is_recording():
        # someone else started recording
        session_settings.set_is_recording(True)
        button_start_recording.disabled = True

    if session_settings.get_is_recording() and not global_settings.get_is_recording():
        # someone else stopped recording
        session_settings.set_is_recording(False)
        button_start_recording.disabled = False
        button_start_recording.name = "view_mission"

async def fake():
    i = 0
    while True:
        d = math.sin(i/200) * 100
        # print(d)
        await asyncio.sleep(0.005)
        i = i + 1
        mqtt_provider.update("14552/DISTANCE_SENSOR", "{\"TimeBootMs\":62910,\"MinDistance\":20,\"MaxDistance\":790,\"CurrentDistance\":" + str(d) + ",\"Type\":0,\"Id\":0,\"Orientation\":25,\"Covariance\":0,\"HorizontalFov\":0,\"VerticalFov\":0,\"Quaternion\":[0,0,0,0]}")


#asyncio.ensure_future(fake())
# 
#live = column(row(custom_map2.figure, min_height=600, sizing_mode="stretch_both"), row(attitude.figure, distance_sensor.figure, min_height=600, sizing_mode="stretch_both"), row(battery.figure, chemical.figure, min_height=600, sizing_mode="stretch_both"), row(radiation.figure, min_height=600, sizing_mode="stretch_both"), sizing_mode="stretch_both")

async def connect_mavsdk_clients():
    for id in mavsdks_by_vehicle_id:
        print(f"connecting mavsdk for {id}")
        await mavsdks_by_vehicle_id[id].connect()
        print(f"{id} connected")

asyncio.ensure_future(connect_mavsdk_clients())

doc.add_root(column(map_row, my_grid, sizing_mode="stretch_both"))
doc.add_periodic_callback(update_plots, global_settings.get_periodic_interval_milliseconds())
doc.add_periodic_callback(update_ui, 500)
