from bokeh.models import ColumnDataSource, DatetimeTickFormatter, Range1d
from bokeh.plotting import figure
import time
import datetime
from influxdb_client import InfluxDBClient
from MQTTMessageProvider import MQTTDataProvider
from GlobalSettings import GlobalSettings

class Chemical:
    def __init__(self, vehicle_ids, x_range = None):
        self.settings = GlobalSettings()
        self.update_rate_hz = int(1)
        self.update_count = (10 / self.update_rate_hz)
        self.roll_over_amount = int(250 / self.update_count)
        self.vehicle_ids = vehicle_ids
        self.source = ColumnDataSource({"x": [], "concentration": [], "dose": [], "bars": []})
        self.datetime_tick_formatter = DatetimeTickFormatter(microseconds = "%H:%M:%S", milliseconds = "%H:%M:%S", seconds = "%H:%M:%S", minutes = "%H:%M:%S", hours = "%H:%M:%S", days="%H:%M:%S",months="%H:%M:%S", years="%H:%M:%S",  minsec = "%H:%M:%S" )
        self.vehicle_id_chemical = self.settings.get_chemical_vehicle_id()

        self.renderers_by_id = {}
        self.renderers_by_id[self.vehicle_id_chemical] = list()

        self.figure = figure(width=950, height=600, syncable=False, x_axis_type='datetime')
        concentration_renderer = self.figure.line("x", "concentration", syncable=False, source=self.source, legend_label="Concentration (mg/m3)", color="red")
        dose_renderer = self.figure.line("x", "dose", syncable=False, source=self.source, legend_label="Dose (mg-min/m3)", color="green")
        bars_renderer = self.figure.line("x", "bars", syncable=False, source=self.source, legend_label="Bars", color="black")
        self.renderers_by_id[self.vehicle_id_chemical].append(concentration_renderer)
        self.renderers_by_id[self.vehicle_id_chemical].append(dose_renderer)
        self.renderers_by_id[self.vehicle_id_chemical].append(bars_renderer)
        
        self.figure.xaxis.axis_label = ""
        self.figure.yaxis.axis_label = ""
        self.figure.xaxis.formatter = self.datetime_tick_formatter
        self.figure.legend.location = "top_left"
        if x_range is not None:
            self.figure.x_range = x_range

    def reset(self):
        self.source.data = {"x": [], "concentration": [], "dose": [], "bars": []}

    def stream(self, mqtt_client: MQTTDataProvider, i: int, vehicle_id: int):
        if i % self.update_count != 0:
            return
        
        if vehicle_id != self.vehicle_id_chemical:
            return

        x = int(time.time()*1000)
        chemical_data = mqtt_client.get_message("CHEMICAL_DETECTOR_DATA", self.vehicle_id_chemical)
        if chemical_data is None:
            return
        concentration = chemical_data["Concentration"]
        dose = chemical_data["Dose"]
        bars = chemical_data["Bars"]
        self.source.stream({"x": [x], "concentration": [concentration], "dose": [dose], "bars": [bars]}, rollover=self.roll_over_amount)

    def set(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):

        if vehicle_id != self.vehicle_id_chemical:
            return

        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "CHEMICAL_DETECTOR_DATA")
        |> filter(fn: (r) => r["vehicleId"] == "{self.vehicle_id_chemical}")
        |> filter(fn: (r) => r["_field"] == "Concentration" or r["_field"] == "Dose" or r["_field"] == "Bars")
        |> aggregateWindow(every: 1s, fn: last, createEmpty: false)
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''
        print(query_string)

        df = query_api.query_data_frame(query_string, org="fhgr")
        if "Concentration" not in df.columns:
            # cant plot
            return

        data = {
                "x": df["_time"].values,
                "concentration": df["Concentration"].values,
                "dose": df["Dose"].values,
                "bars": df["Bars"].values + 10
                }
        print(data)
        self.source.data = data


    def hide_vehicle(self, vehicle_id):
        if vehicle_id != self.vehicle_id_chemical:
            return

        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = False

    def show_vehicle(self, vehicle_id):
        if vehicle_id != self.vehicle_id_chemical:
            return

        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = True
