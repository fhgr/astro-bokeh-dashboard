from bokeh.models import ColumnDataSource, DatetimeTickFormatter, Range1d
from bokeh.plotting import figure
import time
import datetime
from influxdb_client import InfluxDBClient
import math
from MQTTMessageProvider import MQTTDataProvider
import numpy as np
from bokeh.palettes import viridis

class Attitude:

    def __init__(self, vehicle_ids, x_range = None):
        self.update_rate_hz = int(5)
        self.update_count = (10 / self.update_rate_hz)
        self.roll_over_amount = int(250 / self.update_count)
        self.empty_data_structure = {"x": [], "roll": [], "pitch": [], "yaw": [], "angle": []}
        self.sources = {}        
        # create set of colors
        self.vehicle_ids = vehicle_ids
        self.colors = viridis(len(vehicle_ids)*4)

        self.renderers_by_id = {}
        for vehicle_id in self.vehicle_ids:
            self.renderers_by_id[vehicle_id] = list()

        self.figure = figure(width=950, height=600, syncable=False, x_axis_type='datetime')
        self.figure.y_range = Range1d(-3.15,3.15)
        self.figure.xaxis.axis_label = ""
        self.figure.yaxis.axis_label = ""
        self.figure.xaxis.formatter = DatetimeTickFormatter(microseconds = "%H:%M:%S", milliseconds = "%H:%M:%S", seconds = "%H:%M:%S", minutes = "%H:%M:%S", hours = "%H:%M:%S", days="%H:%M:%S",months="%H:%M:%S", years="%H:%M:%S",  minsec = "%H:%M:%S" )
        if x_range is not None:
            self.figure.x_range = x_range

        for vehicle_id in vehicle_ids:
            self.add_vehicle(vehicle_id)

    def add_vehicle(self, vehicle_id):
        i = len(self.sources)
        self.sources[vehicle_id] = ColumnDataSource(self.empty_data_structure)
        roll_renderer = self.figure.line("x", "roll", syncable=False, source=self.sources[vehicle_id], legend_label=f"Roll ({vehicle_id})", color=self.colors[i*4+0])
        pitch_renderer = self.figure.line("x", "pitch", syncable=False, source=self.sources[vehicle_id], legend_label=f"Pitch ({vehicle_id})", color=self.colors[i*4+1])
        yaw_renderer = self.figure.line("x", "yaw", syncable=False, source=self.sources[vehicle_id], legend_label=f"Yaw ({vehicle_id})", color=self.colors[i*4+2])
        angle_renderer = self.figure.line("x", "angle", syncable=False, source=self.sources[vehicle_id], legend_label=f"Angle ({vehicle_id})", color=self.colors[i*4+3])
        self.figure.legend.location = "top_left"
        self.figure.legend.click_policy="hide"
        self.renderers_by_id[vehicle_id].append(roll_renderer)
        self.renderers_by_id[vehicle_id].append(pitch_renderer)
        self.renderers_by_id[vehicle_id].append(yaw_renderer)
        self.renderers_by_id[vehicle_id].append(angle_renderer)
        

    def reset(self):
        for vehicle_id in self.sources:
            self.sources[vehicle_id].data = self.empty_data_structure      

    def stream(self, mqtt_client: MQTTDataProvider, i: int, vehicle_id: int):

        if self.sources.get(vehicle_id) is None:
            return

        if i % self.update_count != 0:
            return
        
        x = int(time.time()*1000)
        attitude = mqtt_client.get_message("ATTITUDE", vehicle_id)
        if attitude is None:
            return
        
        roll = attitude["Roll"]
        pitch = attitude["Pitch"]
        yaw = attitude["Yaw"]
        angle = math.atan(math.sqrt(math.pow(math.tan(pitch or 0), 2) + math.pow(math.tan(roll or 0), 2)))

        # we need all values to plot
        if roll is None or pitch is None or yaw is None or angle is None:
            return
        self.sources[vehicle_id].stream({"x": [x], "roll": [roll], "pitch": [pitch], "yaw": [yaw], "angle": [angle]}, rollover=self.roll_over_amount)


    def set(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):
        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "ATTITUDE")
        |> filter(fn: (r) => r["vehicleId"] == "{vehicle_id}")
        |> filter(fn: (r) => r["_field"] == "Roll" or r["_field"] == "Pitch" or r["_field"] == "Yaw")
        |> aggregateWindow(every: 100ms, fn: last, createEmpty: false)
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''

        df_attitude = query_api.query_data_frame(query_string, org="fhgr")
        if "Roll" not in df_attitude.columns:
            # cant plot
            return
        
        try:
            data = {
                    "x": df_attitude["_time"].values,
                    "roll": df_attitude["Roll"].values,
                    "pitch": df_attitude["Pitch"].values,
                    "yaw": df_attitude["Yaw"].values,
                    "angle": np.arctan(np.sqrt(np.power(np.tan(df_attitude["Pitch"].values), 2) + np.power(np.tan(df_attitude["Roll"].values), 2)))
                }
            self.sources[vehicle_id].data = data
        except:
            print("error calculating attitude from influx. probably no data (wrong sysID?)")

    def hide_vehicle(self, vehicle_id):
        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = False

    def show_vehicle(self, vehicle_id):
        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = True
