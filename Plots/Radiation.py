from bokeh.models import ColumnDataSource, DatetimeTickFormatter, Range1d
from bokeh.plotting import figure
import time
import datetime
from influxdb_client import InfluxDBClient
from GlobalSettings import GlobalSettings
from MQTTMessageProvider import MQTTDataProvider
import numpy as np

class Radiation:
    def __init__(self, vehicle_ids, x_range = None):
        self.settings = GlobalSettings()
        self.update_rate_hz = int(5)
        self.update_count = (10 / self.update_rate_hz)
        self.roll_over_amount = int(250 / self.update_count)
        self.is_live = True
        self.source = ColumnDataSource({"x": [], "rates0": [], "rates1": [], "counts0": [], "counts1": []})
        self.datetime_tick_formatter = DatetimeTickFormatter(microseconds = "%H:%M:%S", milliseconds = "%H:%M:%S", seconds = "%H:%M:%S", minutes = "%H:%M:%S", hours = "%H:%M:%S", days="%H:%M:%S",months="%H:%M:%S", years="%H:%M:%S",  minsec = "%H:%M:%S" )
        self.vehicle_id_radiation = self.settings.get_radiation_vehicle_id()
        self.vehicle_ids = vehicle_ids

        self.renderers_by_id = {}
        self.renderers_by_id[self.vehicle_id_radiation] = list()

        #self.radiation_id = "AAIAAAAAIDc2WDdWUBMAPAA_"
        self.figure = figure(width=950, height=600, syncable=False, x_axis_type='datetime')
        rates0_renderer = self.figure.line("x", "rates0", syncable=False, source=self.source, legend_label="Rates 0 (tbd)", color="red")
        rates1_renderer = self.figure.line("x", "rates1", syncable=False, source=self.source, legend_label="Rates 1 (tbd)", color="yellow")
        counts0_renderer = self.figure.line("x", "counts0", syncable=False, source=self.source, legend_label="Counts 0 (tbd)", color="blue")
        counts1_renderer = self.figure.line("x", "counts1", syncable=False, source=self.source, legend_label="Counts 1 (tbd)", color="green")
        self.renderers_by_id[self.vehicle_id_radiation].append(rates0_renderer)
        self.renderers_by_id[self.vehicle_id_radiation].append(rates1_renderer)
        self.renderers_by_id[self.vehicle_id_radiation].append(counts0_renderer)
        self.renderers_by_id[self.vehicle_id_radiation].append(counts1_renderer)
        
        # self.figure.line("x", "dose", syncable=False, source=self.source, legend_label="Dose (mg-min/m3)", color="green")
        # self.figure.vbar("x", top="bars", syncable=False, width=50, source=self.source, legend_label="Bars", color="black")
        self.figure.xaxis.axis_label = ""
        self.figure.yaxis.axis_label = ""
        self.figure.xaxis.formatter = self.datetime_tick_formatter
        self.figure.legend.location = "top_left"
        self.figure.legend.click_policy="hide"
        
        if x_range is not None:
            self.figure.x_range = x_range

    def reset(self):
        self.source.data = {"x": [], "rates0": [], "rates1": [], "counts0": [], "counts1": []}

    def stream(self, mqtt_client: MQTTDataProvider, i: int, vehicle_id: int):
        if i % self.update_count != 0:
            return
        
        if vehicle_id != self.settings.get_radiation_vehicle_id():
            return

        x = int(time.time()*1000)
        radiation_data = mqtt_client.get_message("RADIATION_DETECTOR_DATA", self.vehicle_id_radiation)
        if radiation_data is None:
            return
        rates0 = radiation_data["Rates0"]
        rates1 = radiation_data["Rates1"]
        counts0 = radiation_data["Counts0"]
        counts1 = radiation_data["Counts1"]
        self.source.stream({"x": [x], "rates0": [rates0], "rates1": [rates1], "counts0": [0], "counts1": [0]}, rollover=self.roll_over_amount)

    def set(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):

        if vehicle_id != self.vehicle_id_radiation:
            return

        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "RADIATION_DETECTOR_DATA")
        |> filter(fn: (r) => r["vehicleId"] == "{self.vehicle_id_radiation}")
        |> filter(fn: (r) => r["_field"] == "Rates0" or r["_field"] == "Rates1" or r["_field"] == "Counts0" or r["_field"] == "Counts1")
        |> aggregateWindow(every: 100ms, fn: last, createEmpty: false)
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''
        print(query_string)

        df = query_api.query_data_frame(query_string, org="fhgr")
        if not "Rates0" in df.columns:
            # cant plot
            return

        data = {
                "x": df["_time"].values,
                "rates0": df["Rates0"].values,
                "rates1": df["Rates1"].values,
                "counts0": np.zeros(len(df["_time"])),
                "counts1": np.zeros(len(df["_time"]))
                }
        self.source.data = data

    # def update(self):
    #     if not self.is_live:
    #         return
        
    #     x = int(time.time()*1000)
    #     roll = self.mqtt_client.get_float("ATTITUDE", "Roll")
    #     pitch = self.mqtt_client.get_float("ATTITUDE", "Pitch")
    #     yaw = self.mqtt_client.get_float("ATTITUDE", "Yaw")
    #     angle = math.atan(math.sqrt(math.pow(math.tan(pitch or 0), 2) + math.pow(math.tan(roll or 0), 2)))

    #     # we need all values to plot
    #     if roll is None or pitch is None or yaw is None or angle is None:
    #         return
    #     self.source.stream({"x": [x], "roll": [roll], "pitch": [pitch], "yaw": [yaw], "angle": [angle]}, rollover=self.roll_over_amount)

    # def set_data(self, data):
    #     print(len(self.source.data["x"]))
    #     self.is_live = False
    #     self.source.data = data



    def hide_vehicle(self, vehicle_id):
        if vehicle_id != self.vehicle_id_radiation:
            return

        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = False

    def show_vehicle(self, vehicle_id):
        if vehicle_id != self.vehicle_id_radiation:
            return

        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = True
