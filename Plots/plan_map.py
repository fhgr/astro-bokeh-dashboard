import datetime
from influxdb_client import InfluxDBClient
import numpy as np
import xyzservices.providers as xyz
from bokeh.models import ColumnDataSource, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, HoverTool, ZoomInTool, ZoomOutTool
from bokeh.plotting import figure
from bokeh.util.hex import hexbin, cartesian_to_axial, axial_to_cartesian
from bokeh.transform import linear_cmap
import pandas as pd
import time
from pyproj import Transformer
from MQTTMessageProvider import MQTTDataProvider
from bokeh.events import PlotEvent
from WaypointManager import WaypointManager
from GlobalSettings import GlobalSettings
        
class PlanMap:
    def __init__(self, vehicle_ids):
        settings = GlobalSettings()
        self.vehicle_ids = vehicle_ids
        self.source_waypoints = ColumnDataSource({"lon": [], "lat": [], "i": []})

        self.default_gps_position = {"Lat": 468565040, "Lon": 95174400}
        self.lon_lat_to_webmercator = Transformer.from_crs("EPSG:4326", "EPSG:3857", always_xy=True)
        self.center_of_sportplatz_lon, self.center_of_sportplatz_lat = self.lon_lat_to_webmercator.transform(9.517440, 46.856504)
        
        self.source_route_planning = ColumnDataSource(data=dict(x=[], y=[]))

        self.figure = figure(toolbar_location="above", output_backend=settings.get_output_backend(), toolbar_sticky=False, syncable=False, x_range=(1.0575e+6, 1.0615e+6), y_range=(5.9178e+6, 5.9192e+6), sizing_mode="scale_both", tools=[], styles=dict(padding="5px 0px 0px 0px"))
        self.figure.add_tile(xyz.OpenStreetMap.CH)

        b1 = self.figure.circle([],[], syncable=False, alpha=1, size=30, fill_color="red")
        c1 = self.figure.patches("x","y", alpha=0.5, source=self.source_route_planning)
        self.figure.circle(x="lon", y="lat", syncable=False, source=self.source_waypoints, legend_label="waypoints", fill_color="yellow", size=5, fill_alpha=0.6)
        self.figure.line(x="lon", y="lat", syncable=False, source=self.source_waypoints, legend_label="waypoints line", line_color="black", line_width=3)

        self.draw_tool = PolyDrawTool(renderers=[c1])
        self.edit_tool =  PolyEditTool(renderers=[c1], vertex_renderer=b1)
        
        self.source_route_planning.on_change("data", self.on_datasource_change)
        
        self.wheel_zoom_tool = WheelZoomTool()
        
        self.figure.add_tools(self.wheel_zoom_tool, PanTool(), ZoomInTool(), ZoomOutTool(), self.draw_tool, self.edit_tool, ResetTool())
        self.figure.toolbar.active_scroll = self.wheel_zoom_tool
        self.figure.yaxis.visible = False
        self.figure.xaxis.visible = False
        self.figure.legend.location = "top_left"
        self.figure.legend.click_policy="hide"
        self.figure.toolbar.logo = None

    def activate_draw(self):
        self.figure.toolbar.active_drag = self.draw_tool

    def on_datasource_change(self, attr, old, new):
        print("Updating drawn path because area changed")
        if len(new["x"]) <= 0 or len(new["x"][0]) < 4:
            return
        elif len(new["x"][0]) == 5:
            new["x"][0] = new["x"][0][:-1]
            new["y"][0] = new["y"][0][:-1]
        
        waypoint_manager = WaypointManager()
        waypoint_manager.build_waypoints("2", new["x"][0], new["y"][0])
        waypoints = waypoint_manager.get_waypoints_x_y("2")
        waypoints_lat_lon = waypoint_manager.get_waypoints_lat_lon("2")
        if not waypoints:
            print("Not drawing path")
            return
        self.clear_waypoints()

        print(waypoints_lat_lon)

        self.set_waypoints(waypoints)
        
    def set_waypoints(self, waypoints):
        index = 1
        for point in waypoints:
            x, y = point
            index = index + 1
            self.source_waypoints.stream({"lat": [y],"lon": [x],"i": [index]})

    def clear_waypoints(self):
        self.source_waypoints.data = {"lon": [], "lat": [], "i": []}

    def get_drawn_area(self):
        print(self.source_route_planning.data)
        return self.source_route_planning.data
