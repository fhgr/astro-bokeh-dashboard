import datetime
from influxdb_client import InfluxDBClient
import numpy as np
import xyzservices.providers as xyz
from bokeh.models import ColumnDataSource,Range1d, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, HoverTool, ZoomInTool, ZoomOutTool
from bokeh.plotting import figure
from bokeh.util.hex import hexbin, cartesian_to_axial, axial_to_cartesian
from bokeh.transform import linear_cmap
import pandas as pd
import time
from pyproj import Transformer
from MQTTMessageProvider import MQTTDataProvider
from bokeh.events import PlotEvent
from WaypointManager import WaypointManager
from GlobalSettings import GlobalSettings

class LiveMap:
    def __init__(self, vehicle_ids, default_lat=46.856504, default_lon=9.517440):
        settings = GlobalSettings()
        self.source_waypoints = ColumnDataSource({"lon": [], "lat": [], "i": []})
        self.vehicle_ids = vehicle_ids
        self.chemical_index = 1
        self.radiation_index = 1
        self.chem_cache = {}
        self.rad_cache = {}
        self.renderers_by_id = {}
        for vehicle_id in self.vehicle_ids:
            self.renderers_by_id[vehicle_id] = list()
        
        self.vehicle_id_chemical = settings.get_chemical_vehicle_id()
        self.vehicle_id_radiation = settings.get_radiation_vehicle_id()

        self.default_gps_position = {"Lat": 468565040, "Lon": 95174400}
        self.lon_lat_to_webmercator = Transformer.from_crs("EPSG:4326", "EPSG:3857", always_xy=True)
        self.map_center_lon_mercator, self.map_center_lat_mercator = self.lon_lat_to_webmercator.transform(default_lon, default_lat)

        self.map_sources = {}
        self.current_position_sources = {}
        for vehicle_id in self.vehicle_ids:
            self.map_sources[vehicle_id] = ColumnDataSource({ "lon": [], "lat": [], "wind_dir": [], "wind_strength": [], "tilt_dir": [], "tilt_strength": []})
            self.current_position_sources[vehicle_id] = ColumnDataSource({"lon": [], "lat": []})
        
        # size (area) of a hex tile. increase to get bigger tiles (but less accuracy)
        self.hex_bin_size = 5

        # grouped data for map plot, do not add data manually
        self.source_chemical_data = ColumnDataSource(hexbin(np.array([]), np.array([]), self.hex_bin_size))
        self.source_radiation_data = ColumnDataSource(hexbin(np.array([]), np.array([]), self.hex_bin_size))

        self.source_route_planning = ColumnDataSource(data=dict(x=[], y=[]))

        chem_tooltips = [
            ("index", "$index"),
            ("Chem", "@counts"),
        ]

        self.chem_text_source = ColumnDataSource({"index":[], "lon": [], "lat": [], "c": []})
        self.radiation_text_source = ColumnDataSource({"index":[], "lon": [], "lat": [], "c": []})

        self.group_by_qr_mean = None

        x_range = (self.map_center_lon_mercator - 2000, self.map_center_lon_mercator + 2000)
        y_range = (self.map_center_lat_mercator - 700, self.map_center_lat_mercator + 700)
        
        # tooltips=chem_tooltips,
        self.figure = figure(toolbar_location="above", output_backend=settings.get_output_backend(),  toolbar_sticky=False, x_axis_type="mercator", y_axis_type="mercator", syncable=False, x_range=x_range, y_range=y_range, sizing_mode="stretch_both", tools=[])
        #self.figure.toolbar.autohide = True

        self.figure.min_height = 250

        self.figure.add_tile(xyz.OpenStreetMap.CH)
        colors = ["red", "green", "blue", "purple", "black", "indigo", "brown"]
        for i, (k,v) in enumerate(self.map_sources.items()):
            path_renderer = self.figure.circle(x="lon", y="lat", syncable=False, source=v, legend_label=f"Path ({k})", fill_color=colors[i], size=5, fill_alpha=0.6)
            wind_renderer = self.figure.ray(x="lon", y="lat", syncable=False, legend_label=f"Wind ({k})", length="wind_strength", angle="wind_dir", angle_units="rad", color="green", alpha=0.5, line_width=2, source=v)
            tilt_renderer = self.figure.ray(x="lon", y="lat", visible=False, syncable=False, legend_label=f"Tilt ({k})", length="tilt_strength", angle="tilt_dir", angle_units="rad", color="blue", alpha=0.5, line_width=2, source=v)
            self.renderers_by_id[k].append(path_renderer)
            self.renderers_by_id[k].append(wind_renderer)
            self.renderers_by_id[k].append(tilt_renderer)

        for i, (k,v) in enumerate(self.current_position_sources.items()):
            self.figure.circle(x="lon", y="lat", syncable=False, source=v, legend_label=f"Current Position ({k})", fill_color=colors[i], size=20, fill_alpha=0.6)

        self.chem_renderer = self.figure.hex_tile(q="q", r="r",  syncable=False, size=self.hex_bin_size, legend_label=f"Chemical ({self.vehicle_id_chemical})", line_color=None, source=self.source_chemical_data, alpha=0.4, fill_color=linear_cmap('counts', 'Viridis256', 0, 20))
        self.chem_renderer.visible = True
        self.rad_renderer =  self.figure.hex_tile(q="q", r="r", syncable=False, size=self.hex_bin_size, legend_label=f"Radiation ({self.vehicle_id_radiation})", line_color=None, source=self.source_radiation_data, alpha=0.4, fill_color=linear_cmap('counts', 'Viridis256', 0, 300))
        self.rad_renderer.visible = True

        self.chem_text_renderer = self.figure.text(x="lon", y="lat", text="c", syncable=False, text_font_size="8pt", source=self.chem_text_source, text_baseline="middle", text_align="center")
        self.radiation_text_renderer = self.figure.text(x="lon", y="lat", text="c", syncable=False, text_font_size="8pt", source=self.radiation_text_source, text_baseline="middle", text_align="center")
        
        #map_fig.line("x", "y", source=self.source_route_planning)
        b1 = self.figure.circle([],[], syncable=False, alpha=1, size=30, fill_color="red")
        c1 = self.figure.patches("x","y", alpha=0.5, source=self.source_route_planning)
        self.figure.circle(x="lon", y="lat", syncable=False, source=self.source_waypoints, legend_label="waypoints", fill_color="yellow", size=5, fill_alpha=0.6)
        self.figure.line(x="lon", y="lat", syncable=False, source=self.source_waypoints, legend_label="waypoints line", line_color="black", line_width=3)

        self.draw_tool = PolyDrawTool(renderers=[c1])
        self.edit_tool =  PolyEditTool(renderers=[c1], vertex_renderer=b1)
        
        #self.source_route_planning.on_change("data", self.on_datasource_change)
        
        self.wheel_zoom_tool = WheelZoomTool()
        
        self.figure.add_tools(self.wheel_zoom_tool, PanTool(), ZoomInTool(), ZoomOutTool(), self.draw_tool, self.edit_tool, ResetTool())
        self.figure.toolbar.active_scroll = self.wheel_zoom_tool
        self.figure.yaxis.visible = False
        self.figure.xaxis.visible = False
        self.figure.legend.location = "top_left"
        self.figure.legend.click_policy="hide"
        self.figure.xgrid.visible = False
        self.figure.ygrid.visible = False
        
    def stream(self, mqtt_client: MQTTDataProvider, i: int, vehicle_id: int):
        if i % 5 == 0:
            self.stream_chemical(mqtt_client, vehicle_id)
            self.stream_radiation(mqtt_client, vehicle_id)
        self.stream_gps(mqtt_client, i, vehicle_id)

    def set(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):
        self.set_chemical(influx_client, start_time, end_time, vehicle_id)
        self.set_radiation(influx_client, start_time, end_time, vehicle_id)
        self.set_gps(influx_client, start_time, end_time, vehicle_id)

    def stream_radiation(self, mqtt_client: MQTTDataProvider, vehicle_id: int):

        if vehicle_id != self.vehicle_id_radiation:
            return
        
        # grab newest data
        global_position_int = mqtt_client.get_message("GLOBAL_POSITION_INT", vehicle_id) or self.default_gps_position
        if global_position_int is None:
            return
        lon = global_position_int["Lon"]
        lat = global_position_int["Lat"]
        radiation_data = mqtt_client.get_message("RADIATION_DETECTOR_DATA", self.vehicle_id_radiation)
        if radiation_data is None:
            return
        rates = radiation_data["Rates0"]
        # we need all data to continue
        if lat is None or lon is None or rates is None:
            return
        
        lon_mercator, lat_mercator = self.lon_lat_to_webmercator.transform(lon / 10000000.0, lat / 10000000.0)
        q, r = cartesian_to_axial(lon_mercator, lat_mercator, size=self.hex_bin_size, orientation="pointytop", aspect_scale=1)
        q = int(q)
        r = int(r)
        lat, lon = axial_to_cartesian(q,r, size=self.hex_bin_size,orientation="pointytop", aspect_scale=1)
        entry = self.rad_cache.get((q,r))
        if entry is None:
            # stream
            self.rad_cache[(q,r)] = {}
            self.rad_cache[(q,r)]["value"] = rates
            self.rad_cache[(q,r)]["count"] = 1
            self.rad_cache[(q,r)]["index"] = self.radiation_index
            self.source_radiation_data.stream({"index": [self.radiation_index],"q": [q],"r": [r],"counts": [rates] })
            self.radiation_text_source.stream({"index": [self.radiation_index], "lon": [lat],"lat": [lon],"c": ["{:.1f}".format(rates)]})
            self.radiation_index = self.radiation_index + 1

        else:
            # patch
            index = self.rad_cache[(q,r)]["index"] - 1
            self.rad_cache[(q,r)]["value"] = (entry["value"] * entry["count"] + rates) / (entry["count"] + 1)
            self.rad_cache[(q,r)]["count"] = entry["count"] + 1
            self.source_radiation_data.patch({"counts": [(index, self.rad_cache[(q,r)]["value"])] })
            self.radiation_text_source.patch({"c": [(index, ["{:.1f}".format(self.rad_cache[(q,r)]["value"])])]})

    def stream_chemical(self, mqtt_client: MQTTDataProvider, vehicle_id: int):
        if vehicle_id != self.vehicle_id_chemical:
            return
        
        # grab newest data
        global_position_int = mqtt_client.get_message("GLOBAL_POSITION_INT", vehicle_id) or self.default_gps_position
        if global_position_int is None:
            print("pos is none, return")
            return
        lon = global_position_int["Lon"]
        lat = global_position_int["Lat"]
        #chemical_detector_data = mqtt_client.get_message("CHEMICAL_DETECTOR_DATA", vehicle_id)
        chemical_detector_data = mqtt_client.get_message("CHEMICAL_DETECTOR_DATA", self.vehicle_id_chemical)

        if chemical_detector_data is None:
            return
        concentration = chemical_detector_data["Concentration"]

        # we need all data to continue
        if lat is None or lon is None or concentration is None:
            return
        
        lon_mercator, lat_mercator = self.lon_lat_to_webmercator.transform(lon / 10000000.0, lat / 10000000.0)
        q, r = cartesian_to_axial(lon_mercator, lat_mercator, size=self.hex_bin_size, orientation="pointytop", aspect_scale=1)
        q = int(q)
        r = int(r)
        lat, lon = axial_to_cartesian(q,r, size=self.hex_bin_size,orientation="pointytop", aspect_scale=1)
        entry = self.chem_cache.get((q,r))
        if entry is None:
            # stream
            self.chem_cache[(q,r)] = {}
            self.chem_cache[(q,r)]["value"] = concentration
            self.chem_cache[(q,r)]["count"] = 1
            self.chem_cache[(q,r)]["index"] = self.chemical_index
            self.source_chemical_data.stream({"index": [self.chemical_index],"q": [q],"r": [r],"counts": [concentration] })
            self.chem_text_source.stream({"index": [self.chemical_index], "lon": [lat],"lat": [lon],"c": ["{:.1f}".format(concentration)]})
            self.chemical_index = self.chemical_index + 1

        else:
            # patch
            index = self.chem_cache[(q,r)]["index"] - 1
            self.chem_cache[(q,r)]["value"] = (entry["value"] * entry["count"] + concentration) / (entry["count"] + 1)
            self.chem_cache[(q,r)]["count"] = entry["count"] + 1
            self.source_chemical_data.patch({"counts": [(index, self.chem_cache[(q,r)]["value"])] })
            self.chem_text_source.patch({"c": [(index, ["{:.1f}".format(self.chem_cache[(q,r)]["value"])])]})

    def stream_gps(self, mqtt_client: MQTTDataProvider, i: int, vehicle_id: int):
        global_position_int = mqtt_client.get_message("GLOBAL_POSITION_INT", vehicle_id)
        if global_position_int is None:
            #print("gps none")
            return
        lon = global_position_int["Lon"]
        lat = global_position_int["Lat"]
        #print(f"stream gps {vehicle_id}")
        # we need all data to continue
        if lat is None or lon is None:
            print("lat is none or lon is none, return")
            return

        # convert lat lon (divided by 10000000 because raw value is INT) (-> e.g. 9.821, 43.542) to mercator coordinates
        lon_mercator, lat_mercator = self.lon_lat_to_webmercator.transform(lon / 10000000.0, lat / 10000000.0)

        self.current_position_sources[vehicle_id].stream({"lon": [lon_mercator], "lat": [lat_mercator]}, rollover=1)

        if i % 5 != 0:
            # only stream position, but no attitude and wind
            return

        attitude = mqtt_client.get_message("ATTITUDE", vehicle_id)

        # alpha = "tilt" stärke
        alpha = np.nan

        # beta = winkel / richtung ray
        beta = 0

        if attitude is not None:
            roll = attitude["Roll"]
            pitch = attitude["Pitch"]
            alpha = np.arctan(np.sqrt(np.power(np.tan(roll),2)+np.power(np.tan(pitch),2)))
            alpha = alpha * 100
            beta = np.arctan2(roll, pitch)

        wind_cov = mqtt_client.get_message("WIND_COV", vehicle_id)
        angle = 0
        magnitude = np.nan
        if wind_cov is not None:
            wind_x = wind_cov["WindX"]
            wind_y = wind_cov["WindY"]
            angle = np.arctan2(wind_x, wind_y) / np.pi * 180.0 
            magnitude = np.sqrt(np.power(wind_x, 2) + np.power(wind_y, 2))


            self.map_sources[vehicle_id].stream({"lon": [lon_mercator], "lat": [lat_mercator], "wind_dir": [angle], "wind_strength": [magnitude], "tilt_dir": [beta], "tilt_strength": [alpha]}, rollover=50)


    def set_radiation(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):
        
        if vehicle_id != self.vehicle_id_radiation:
            return
        
        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "GLOBAL_POSITION_INT" or r["_measurement"] == "RADIATION_DETECTOR_DATA")
        |> filter(fn: (r) => r["vehicleId"] == "{self.vehicle_id_radiation}")
        |> filter(fn: (r) => r["_field"] == "Lat" or r["_field"] == "Lon" or r["_field"] == "Rates0")
        |> aggregateWindow(every: 300ms, fn: last, createEmpty: false)
        |> toFloat()
        |> group()
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''

        print(query_string)
        df_rad = query_api.query_data_frame(query_string, org="fhgr")

        if "Lat" not in df_rad.columns or "Lon" not in df_rad.columns:
            return
        
        lon_mercator, lat_mercator = self.lon_lat_to_webmercator.transform(df_rad["Lon"].values / 10000000.0, df_rad["Lat"].values / 10000000.0)
        
        x = lon_mercator
        y = lat_mercator
        c = df_rad["Rates0"].values if "Rates0" in df_rad.columns else np.zeros(len(x))

        q, r = cartesian_to_axial(x, y, size=self.hex_bin_size, orientation="pointytop", aspect_scale=1)
        radiation_data = pd.DataFrame({"q": q, "r": r, "c": c})
        self.source_radiation_data.data = radiation_data.groupby(['q', 'r'])["c"].mean().reset_index(name="counts")


    def set_chemical(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):
        # reset previous chem data
        # self.chemical_data = pd.DataFrame(data={"q": [], "r": [], "c": []})
        if vehicle_id != self.vehicle_id_chemical:
            return

        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "GLOBAL_POSITION_INT" or r["_measurement"] == "CHEMICAL_DETECTOR_DATA")
        |> filter(fn: (r) => r["vehicleId"] == "{self.vehicle_id_chemical}")
        |> filter(fn: (r) => r["_field"] == "Lat" or r["_field"] == "Lon" or r["_field"] == "Concentration")
        |> aggregateWindow(every: 300ms, fn: last, createEmpty: false)
        |> toFloat()
        |> group()
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''

        print(query_string)
        df_chem = query_api.query_data_frame(query_string, org="fhgr")
        

        if "Concentration" not in df_chem.columns or "Lon" not in df_chem.columns:
            print("cant plot conc")
            # cant plot
            return
        
        df_chem = df_chem[df_chem["Concentration"] >= 0]

        lon_mercator, lat_mercator = self.lon_lat_to_webmercator.transform(df_chem["Lon"].values / 10000000.0, df_chem["Lat"].values / 10000000.0)
        
        x = lon_mercator
        y = lat_mercator
        c = df_chem["Concentration"].values

        # TODO: size is dependend on size specified above for hex tiles
        q, r = cartesian_to_axial(x, y, size=self.hex_bin_size, orientation="pointytop", aspect_scale=1)

        # append new data (1 row) to existing data (warn: .append() is no longer supported by pandas, what a shame)
        self.chemical_data = pd.concat([self.chemical_data, pd.DataFrame({"q": q, "r": r, "c": c})])
        # if group_by_qr_mean is not None:
        #     affected_rows = group_by_qr_mean.loc[group_by_qr_mean["q"] == q[0] and group_by_qr_mean["r"] == r[0]]
        #     if affected_rows is not None:
        #         print("OK?")
        #         print(affected_rows)
        # group by 'q' and 'r' (-> position, according to bin size) then average all concentration measurements and write to new column 'counts'
        self.group_by_qr_mean = self.chemical_data.groupby(['q', 'r'])["c"].mean().reset_index(name="counts")

        
        
        # update data with WHOLE new dataset
        #source.patch

        lat, lon = axial_to_cartesian(self.group_by_qr_mean["q"].values, self.group_by_qr_mean["r"].values, size=self.hex_bin_size,orientation="pointytop", aspect_scale=1)

        self.chem_text_source.data = {"lon": lat,"lat": lon,"c": list(map(lambda n: "{:.1f}".format(n), self.group_by_qr_mean["counts"].values))}
        self.source_chemical_data.data = self.group_by_qr_mean

    def set_gps(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):
        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "GLOBAL_POSITION_INT" or r["_measurement"] == "WIND_COV" or r["_measurement"] == "ATTITUDE")
        |> filter(fn: (r) => r["vehicleId"] == "{vehicle_id}")
        |> filter(fn: (r) => r["_field"] == "Lat" or r["_field"] == "Lon" or r["_field"] == "WindX" or r["_field"] == "WindY" or r["_field"] == "Roll" or r["_field"] == "Pitch")
        |> aggregateWindow(every: 100ms, fn: last, createEmpty: false)
        |> toFloat()
        |> group()
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''

        print(query_string)
        df_gps = query_api.query_data_frame(query_string, org="fhgr")
        
        #or "Yaw" not in df_gps.columns
        if "Lon" not in df_gps.columns:
            # cant plot
            return
        
        wind_dirs = np.zeros(len(df_gps["Lon"].values)) + 0
        wind_strengths = np.full(len(df_gps["Lon"].values), np.nan)

        # beta = winkel / richtung ray
        # alpha = "tilt" stärke
        betas = np.zeros(len(df_gps["Roll"].values)) + 0
        alphas = np.full(len(df_gps["Roll"].values), np.nan)

        if "Pitch" in df_gps.columns and "Roll" in df_gps.columns:
            for i in range(len(betas)):
                roll = df_gps["Roll"].values[i]
                pitch = df_gps["Pitch"].values[i]
                alpha = np.arctan(np.sqrt(np.power(np.tan(roll),2)+np.power(np.tan(pitch),2)))
                alpha = alpha * 100
                beta = np.arctan2(roll, pitch)
                betas[i] = beta
                alphas[i] = alpha

        if "WindX" in df_gps.columns:
            for i in range(len(wind_dirs)):
                wind_x = df_gps["WindX"].values[i]
                wind_y = df_gps["WindY"].values[i]
                angle = np.arctan2(wind_x, wind_y) / np.pi
                magnitude = np.sqrt(np.power(wind_x, 2) + np.power(wind_y, 2))
                #print(f'angle: {angle}')
                #print(f'yaw: {df_gps["Yaw"].values[i]}')
                wind_dirs[i] = angle# + df_gps["Yaw"].values[i] or 0
                wind_strengths[i] = magnitude

        try:
            lon_mercator, lat_mercator = self.lon_lat_to_webmercator.transform(df_gps["Lon"].values / 10000000.0, df_gps["Lat"].values / 10000000.0)

            data = {
                    "lat": lat_mercator,
                    "lon": lon_mercator,
                    "wind_dir": wind_dirs,
                    "wind_strength": wind_strengths,
                    "tilt_dir": betas,
                    "tilt_strength": alphas
                }
            self.map_sources[vehicle_id].data = data
        except:
            print("error calculating position / wind from influx. probably no data (wrong sysID?)")
    
    def hide_vehicle(self, vehicle_id):
        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = False
        if self.vehicle_id_chemical == vehicle_id:
            self.set_chemical_visibility(False)
        if self.vehicle_id_radiation == vehicle_id:
            self.set_radiation_visibility(False)

    def show_vehicle(self, vehicle_id):
        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = True
        if self.vehicle_id_chemical == vehicle_id:
            self.set_chemical_visibility(True)
        if self.vehicle_id_radiation == vehicle_id:
            self.set_radiation_visibility(True)

    def get_chemical_visibility(self) -> bool:
        return self.chem_renderer.visible
    
    def get_radiation_visibility(self) -> bool:
        return self.rad_renderer.visible
    
    def set_chemical_visibility(self, value: bool):
        self.chem_renderer.visible = value
        self.chem_text_renderer.visible = value

    def set_radiation_visibility(self, value: bool):
        self.rad_renderer.visible = value
        self.radiation_text_renderer.visible = value