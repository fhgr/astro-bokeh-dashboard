from bokeh.models import ColumnDataSource, DatetimeTickFormatter, Range1d
from bokeh.plotting import figure
import time
import datetime
from influxdb_client import InfluxDBClient
from MQTTMessageProvider import MQTTDataProvider

class Battery:
    def __init__(self, vehicle_ids, x_range = None):
        self.update_rate_hz = int(1)
        self.update_count = (10 / self.update_rate_hz)
        self.roll_over_amount = int(250 / self.update_count)

        self.is_live = True
        self.sources = {}
        self.vehicle_ids = vehicle_ids

        self.renderers_by_id = {}
        for vehicle_id in self.vehicle_ids:
            self.renderers_by_id[vehicle_id] = list()

        for vehicle_id in self.vehicle_ids:
            self.sources[vehicle_id] = ColumnDataSource({"x": [], "battery_remaining": []})

        self.datetime_tick_formatter = DatetimeTickFormatter(microseconds = "%H:%M:%S", milliseconds = "%H:%M:%S", seconds = "%H:%M:%S", minutes = "%H:%M:%S", hours = "%H:%M:%S", days="%H:%M:%S",months="%H:%M:%S", years="%H:%M:%S",  minsec = "%H:%M:%S" )
        self.figure = figure(width=950, height=600, syncable=False, x_axis_type='datetime')
        for i, (k,v) in enumerate(self.sources.items()):
            battery_renderer = self.figure.line("x", "battery_remaining", syncable=False, source=v, legend_label=f"Battery Remaining (%) ({k})", color="purple")
            self.renderers_by_id[k].append(battery_renderer)

        self.figure.xaxis.axis_label = ""
        self.figure.yaxis.axis_label = ""
        self.figure.xaxis.formatter = self.datetime_tick_formatter
        self.figure.legend.location = "top_left"
        self.figure.legend.click_policy="hide"
        if x_range is not None:
            self.figure.x_range = x_range

    def reset(self):
        for i, (k,s) in enumerate(self.sources.items()):
            s.data = {"x": [], "battery_remaining": []}

    def stream(self, mqtt_client: MQTTDataProvider, i: int, vehicle_id: int):     
        if i % self.update_count != 0:
            return   
        x = int(time.time()*1000)
        battery_status = mqtt_client.get_message("BATTERY_STATUS", vehicle_id)
        if battery_status is None:
            return
        battery_remaining = battery_status["BatteryRemaining"]
        #print(f"battery: {battery_status}")
        self.sources[vehicle_id].stream({"x": [x], "battery_remaining": [battery_remaining]}, rollover=self.roll_over_amount)

    def set(self, influx_client: InfluxDBClient, start_time: datetime.datetime, end_time: datetime.datetime, vehicle_id: int):
        date_from = int(time.mktime(start_time.timetuple()))
        date_to = int(time.mktime(end_time.timetuple()))
        
        query_api = influx_client.query_api()

        query_string = f'''
        from(bucket:"mavlink")
        |> range(start: {date_from}, stop: {date_to})
        |> filter(fn: (r) => r["_measurement"] == "BATTERY_STATUS")
        |> filter(fn: (r) => r["vehicleId"] == "{vehicle_id}")
        |> filter(fn: (r) => r["_field"] == "BatteryRemaining")
        |> aggregateWindow(every: 1s, fn: last, createEmpty: false)
        |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
        '''

        df = query_api.query_data_frame(query_string, org="fhgr")
        if "BatteryRemaining" not in df.columns:
            # cant plot
            return
        data = {
                "x": df["_time"].values,
                "battery_remaining": df["BatteryRemaining"].values
                }
        self.sources[vehicle_id].data = data

    def hide_vehicle(self, vehicle_id):
        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = False

    def show_vehicle(self, vehicle_id):
        if vehicle_id in self.vehicle_ids:
            for renderer in self.renderers_by_id[vehicle_id]:
                renderer.visible = True
