import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid
from bokeh.models import ColumnDataSource, CustomJS, TabPanel, Tabs, Label, TextInput, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.map import Map
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial
import time

doc = curdoc()


mission_manager = MissionManagerV2()
completed_missions = mission_manager.get_completed_missions()
planned_missions = mission_manager.get_planned_missions()

start_time = datetime.datetime.now()

planned_missions_header = Div(text="<h3>Planned Missions:</h3>")
completed_missions_header = Div(text="<h3>Completed Missions:</h3>")
completed_missions_elements = list()
planned_missions_elements = list()

add_new_mission_button = Button(label="Create new Mission", button_type="success")
add_new_mission_button.js_on_click(CustomJS(args={}, code='window.location.href = "new_mission"'))

def delete_button_clicked(mission_id):
    mission_manager.delete_mission(mission_id)

for m in planned_missions:
    run_button = Button(label="Run", button_type="primary", align="center")
    run_button.js_on_click(CustomJS(args=dict(id=m["id"]), code='window.location.href = "live?id=" + id'))
    delete_button = Button(label="Delete", button_type="danger", align="center")
    delete_button.js_on_click(CustomJS(args={}, code='setTimeout(() => {location.reload();}, 500);'))
    delete_button.on_click(partial(delete_button_clicked, mission_id=m["id"]))

    mission_row = row(TextInput(disabled=True, value=m["name"]), run_button, delete_button, sizing_mode="scale_width")
    planned_missions_elements.append(mission_row)

for m in completed_missions:
    view_button = Button(label="View", button_type="success", align="center")
    view_button.js_on_click(CustomJS(args=dict(id=m["id"]), code='window.location.href = "view_mission?id=" + id'))
    delete_button = Button(label="Delete", button_type="danger", align="center")
    delete_button.js_on_click(CustomJS(args={}, code='setTimeout(() => {location.reload();}, 500);'))
    delete_button.on_click(partial(delete_button_clicked, mission_id=m["id"]))

    mission_row = row(TextInput(disabled=True, value=m["name"]), view_button, delete_button, sizing_mode="scale_width")
    completed_missions_elements.append(mission_row)

elements = column(planned_missions_header, add_new_mission_button, column(children=planned_missions_elements), completed_missions_header, column(children=completed_missions_elements), align="center", margin=5)

doc.add_root(elements)
