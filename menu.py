import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid
from bokeh.models import ColumnDataSource, CustomJS, TabPanel, Tabs, Label, TextInput, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial
import time

doc = curdoc()


view_live_button = Button(label="View Live Data", height=70, width=300, button_type="primary", align="center")
view_live_button.js_on_click(CustomJS(args={}, code='window.location.href = "live"'))

missions_button = Button(label="Missions Menu", height=70, width=300, button_type="primary", align="center")
# missions_button.js_on_click(CustomJS(args={}, code='window.location.href = "missions"'))
missions_button.js_on_change("name", CustomJS(args={}, code='window.location.href = "missions"'))

settings_button = Button(label="Settings", height=70, width=300, button_type="primary", align="center")
# settings_button.js_on_click(CustomJS(args={}, code='window.location.href = "settings"'))
def settings_button_clicked():
    missions_button.name = "test"
settings_button.on_click(settings_button_clicked)

elements = column(view_live_button, missions_button, settings_button, align="center", styles=dict(padding="10px"), sizing_mode="stretch_both")

doc.add_root(elements)
