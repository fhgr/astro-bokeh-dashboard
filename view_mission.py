import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid, gridplot
from bokeh.models import ColumnDataSource, TabPanel, Tabs, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.live_map import LiveMap
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.radiation import Radiation
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
from WaypointManager import WaypointManager
import app_hooks
from asyncio import Task
from MissionManager import MissionManagerV2
from GlobalSettings import GlobalSettings
from SessionSettings import SessionSettings
from mavsdk import System
from mavsdk import mission
from mavsdk import camera
from mavsdk import camera_server_pb2
from functools import partial
import time

doc = curdoc()

settings = GlobalSettings()
session = SessionSettings()
session.set_is_main(settings.get_is_main())
mission_manager = MissionManagerV2()

mission_id = doc.session_context.request.arguments["id"][0].decode("utf-8") if "id" in doc.session_context.request.arguments else mission_manager.get_latest_mission_id()

if not "id" in doc.session_context.request.arguments:
    print("no mission id supplied, grab latest")
    mission_id = mission_manager.get_latest_mission_id()

# mission_id = doc.session_context.request.arguments["id"][0].decode("utf-8")

print(f"Requested mission id: {mission_id}")

mission_manager = MissionManagerV2()
requested_mission = mission_manager.get_mission_by_id(mission_id)
start = datetime.datetime.strptime(requested_mission["start_time"], '%Y-%m-%d %H:%M:%S.%f')
end =  datetime.datetime.strptime(requested_mission["end_time"], '%Y-%m-%d %H:%M:%S.%f')

is_dev_mode = False

influx_is_connected = False
if not is_dev_mode:
    try:
        influx_client = InfluxDBClient(url=settings.get_influx_address(), token=settings.get_influx_token(), org="fhgr")
        influx_is_connected = True
    except:
        pass

vehicle_ids = requested_mission["vehicle_ids"]
if len(vehicle_ids) <= 0:
    vehicle_ids = ["14551", "14552"] 

def get_latest_gps_pos(start_time, end_time):
    date_from = int(time.mktime(start_time.timetuple()))
    date_to = int(time.mktime(end_time.timetuple()))
    
    query_api = influx_client.query_api()

    query_string = f'''
    from(bucket:"mavlink")
    |> range(start: {date_from}, stop: {date_to})
    |> filter(fn: (r) => r["_measurement"] == "GLOBAL_POSITION_INT")
    |> filter(fn: (r) => r["_field"] == "Lat" or r["_field"] == "Lon")
    |> aggregateWindow(every: 100ms, fn: last, createEmpty: false)
    |> pivot(rowKey:["_time"], columnKey:["_field"], valueColumn:"_value")
    '''
    df_gps = query_api.query_data_frame(query_string, org="fhgr")

    print(df_gps)

    if "Lon" not in df_gps.columns or df_gps["Lon"].values[-1] <= 0:
        return 9.517440, 46.856504
        # cant plot
    print(df_gps["Lon"])
    lon = df_gps["Lon"].values[-1]
    lat = df_gps["Lat"].values[-1]

    return lon / 10000000.0, lat / 10000000.0



# UI elements
attitude = Attitude(vehicle_ids)
distance_sensor = DistanceSensor(vehicle_ids, attitude.figure.x_range)
battery = Battery(vehicle_ids, attitude.figure.x_range)
chemical = Chemical(vehicle_ids, attitude.figure.x_range)
radiation = Radiation(vehicle_ids, attitude.figure.x_range)
lon, lat = get_latest_gps_pos(start, end)
custom_map2 = LiveMap(vehicle_ids, lat, lon)


plots = [attitude, distance_sensor, battery, chemical, radiation, custom_map2]


def load_mission(start, end):
    settings.set_is_live(False)
    for p in plots:
        #p.reset()
        for v in vehicle_ids:
            p.set(influx_client, start, end, v)


load_mission(start,end)


map_row = row(custom_map2.figure, sizing_mode="scale_both")
my_grid = gridplot([[attitude.figure, distance_sensor.figure, battery.figure], [chemical.figure, radiation.figure]], toolbar_location='right',toolbar_options=dict(logo=None), sizing_mode="stretch_both")


# historical = column(row(custom_map2.figure, min_height=600, sizing_mode="stretch_both"), row(attitude.figure, distance_sensor.figure, min_height=600, sizing_mode="stretch_both"), row(battery.figure, chemical.figure, min_height=600, sizing_mode="stretch_both"), row(radiation.figure, min_height=600, sizing_mode="stretch_both"), sizing_mode="stretch_both")
doc.add_root(column(map_row, my_grid, sizing_mode="stretch_both"))
