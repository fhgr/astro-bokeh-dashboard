class GlobalSettings(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(GlobalSettings, cls).__new__(cls)
            cls.instance.__initialized = False
        return cls.instance

    def __init__(self):
        if self.__initialized:
            return
        self.__initialized = True
        self.__is_main_claimed = False

        # below are default values
        self.__is_readonly = True
        self.__is_mission_running = False
        self.__chemical_vehicle_id = "14551"
        self.__radiation_vehicle_id = "14552"
        self.__server_ip = "192.168.1.24"
        self.__influx_address = f"http://{self.__server_ip}:8086"
        self.__influx_token = "g6Ra9iRmkL1307v36xtSEADVeenZETBduEaP6pe-cY-nKNBzd4U411Q7KmkSOWHSqR5rbHJTxNYJsZn3DQjJ9A=="
        self.__mqtt_address = self.__server_ip
        self.__is_live = True
        self.__is_recording = False
        self.__output_backend = "canvas"
        #self.__output_backend = "webgl"
        self.__periodic_interval_milliseconds = 100
        self.can_arm = False

    def get_periodic_interval_milliseconds(self):
        return self.__periodic_interval_milliseconds

    def get_chemical_vehicle_id(self):
        return self.__chemical_vehicle_id
    
    def set_chemical_vehicle_id(self, vehicle_id: str):
        self.__chemical_vehicle_id = vehicle_id
        
    def get_radiation_vehicle_id(self):
        return self.__radiation_vehicle_id
    
    def set_radiation_vehicle_id(self, vehicle_id: str):
        self.__radiation_vehicle_id = vehicle_id    
    
    def get_is_mission_running(self):
        return self.__is_mission_running

    def set_is_mission_running(self, value: bool):
        self.__is_mission_running = value
    
    def get_influx_address(self):
        return self.__influx_address
    
    def set_influx_address(self, address: str):
        self.__influx_address = address

    def get_mqtt_address(self):
        return self.__mqtt_address
    
    def set_mqtt_address(self, address: str):
        self.__mqtt_address = address

    def get_server_ip(self):
        return self.__server_ip
    
    def set_server_ip(self, server_ip: str):
        self.__server_ip = server_ip

    def get_influx_token(self):
        return self.__influx_token
    
    def set_influx_token(self, token: str):
        self.__influx_token = token

    def get_is_live(self):
        return self.__is_live
    
    def set_is_live(self, value: bool):
        self.__is_live = value

    def set_is_recording(self, value: bool):
        self.__is_recording = value

    def get_is_recording(self):
        return self.__is_recording

    def get_is_main(self):
        if not self.__is_main_claimed:
            self.__is_main_claimed = True
            return True
        return False
    
    def set_output_backend(self, value):
        self.__output_backend = value

    def get_output_backend(self):
        return self.__output_backend
    