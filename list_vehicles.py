from bokeh.layouts import column, row
from bokeh.models import CustomJS, TextInput, Button, Div
from bokeh.plotting import curdoc
from VehicleManager import VehicleManager
from functools import partial

doc = curdoc()


vehicle_manager = VehicleManager()

vehicles = vehicle_manager.get_all_vehicles()

vehicles_header = Div(text="<h3>Registered Vehicles:</h3>")
vehicles_elements = list()

add_new_vehicle_button = Button(label="Create new Vehicle", button_type="success")
add_new_vehicle_button.js_on_click(CustomJS(args={}, code='window.location.href = "add_vehicle"'))

def delete_vehicle(vehicle):
    vehicle_manager.delete_vehicle(vehicle)

def update_vehicle(vehicle,vehicle_name, vehicle_id, vehicle_mavsdk):
    print(vehicle)
    print(vehicle_name)
    print(vehicle_id)
    print(vehicle_mavsdk)
    vehicle["id"] = vehicle_id.value
    vehicle["name"] = vehicle_name.value
    vehicle["mavsdk_port"] = vehicle_mavsdk.value
    vehicle_manager.save_vehicle(vehicle)

for vehicle in vehicles:
    delete_button = Button(label="Delete",  button_type="danger", align="end")
    delete_button.js_on_click(CustomJS(args={}, code='setTimeout(() => {location.reload();}, 500);'))
    delete_button.on_click(partial(delete_vehicle, vehicle))
    edit_button = Button(label="Update",  button_type="primary", align="end")

    vehicle_name = TextInput(disabled=False, title="Name", value=vehicle["name"])
    vehicle_id = TextInput(disabled=False, title="Vehicle ID (Port)", value=vehicle["id"])
    vehicle_mavsdk = TextInput(disabled=False, title="MAVSDK Port (0 to disable)", value=vehicle["mavsdk_port"])

    edit_button.on_click(partial(update_vehicle, vehicle, vehicle_name, vehicle_id, vehicle_mavsdk))


    vehicle_row = row(vehicle_name, vehicle_id, vehicle_mavsdk, edit_button, delete_button, sizing_mode="scale_width")
    vehicles_elements.append(vehicle_row)


elements = column(vehicles_header, add_new_vehicle_button, column(children=vehicles_elements), align="center", margin=5)

doc.add_root(elements)
