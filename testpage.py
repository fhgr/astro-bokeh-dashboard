import datetime
import numpy as np
import xyzservices.providers as xyz
from bokeh.layouts import column, row, grid
from bokeh.models import CustomJS, ColumnDataSource, TabPanel, Tabs, RadioGroup, Button, AutocompleteInput, Div, CheckboxGroup, Select, MultiChoice, MultiSelect, DatetimeTickFormatter, Range1d, Tool, ResetTool, WheelZoomTool, PanTool, PolyDrawTool, PolyEditTool, Toggle
from bokeh.driving import count
from bokeh.palettes import Muted3 as color
from bokeh.plotting import figure, curdoc
from bokeh.util.hex import hexbin, cartesian_to_axial
from bokeh.transform import linear_cmap 
import pandas as pd
from bokeh.layouts import layout
import time
import math
from MQTTMessageProvider import MQTTDataProvider
from bokeh.util.compiler import TypeScript
from bokeh.core.properties import Instance
from Plots.map import Map
from Plots.attitude import Attitude
from Plots.battery import Battery
from Plots.chemical import Chemical
from Plots.distance_sensor import DistanceSensor
from influxdb_client import InfluxDBClient, Point
import asyncio
import app_hooks
from asyncio import Task
from MissionManager import MissionManager
from mavsdk import System
from functools import partial


# singleton - only 1 instance per server
mqtt_provider = MQTTDataProvider()

doc = curdoc()

main_button_label = "View current mission" if mqtt_provider.isRunning() else "Plan new mission"
main_button = Button(label=main_button_label, width=200, height=100, align="center")
main_button.js_on_click(CustomJS(args={}, code="""
    window.location.href = "astro-bokeh-dashboard" 
"""))


fleet_button = Button(label="Manage fleet")
fleet_button.js_on_click(CustomJS(args={}, code="""
    window.location.href = "fleet-management"
"""))

doc.add_root(column(main_button, fleet_button, sizing_mode="stretch_width"))