import paho.mqtt.client as mqtt
from paho.mqtt.client import MQTTMessage
import json
import time
from GlobalSettings import GlobalSettings

class MQTTDataProvider(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(MQTTDataProvider, cls).__new__(cls)
            cls.instance.__initialized = False
        return cls.instance

    def __init__(self):
        if self.__initialized:
            return
        self.__initialized = True

        ip = "192.168.1.24"
        port = 1883
        auto_connect = False

        self.__messages = {}
        self.__is_running = False
        self.client = mqtt.Client()
        self.__is_connected = False
        self.client.on_connect = self.__on_connect
        self.client.on_message = self.__on_message
        self.__ip = ip
        self.__port = port
        self.__active_vehicles = []
        self.__is_discovering = False
        if auto_connect:
            self.client.connect(ip, port, 60)
            self.client.loop_start()
            self.__is_connected = True

    def isRunning(self):
        return self.__is_running
    
    def stop(self):
        self.__is_running = False
        self.client.disconnect()
        self.client.loop_stop()
        self.__is_connected = False

    def __discover_devices(self):

        if self.__active_vehicles is not None and len(self.__active_vehicles) > 1:
            print("Skipping discovery (already done)")
            return

        discovery_time_seconds = 8
        print(f"MQTT: Discovering devices for {discovery_time_seconds} seconds")
        self.__is_discovering = True
        t_end = time.time() + discovery_time_seconds
        while time.time() < t_end:
            pass
        print(f"MQTT: Stopping device discovery. Found {len(self.__active_vehicles)} vehicles")
        for v in self.__active_vehicles:
            is_chemical = self.get_message("CHEMICAL_DETECTOR_DATA", v) is not None
            is_radiation = self.get_message("RADIATION_DETECTOR_DATA", v) is not None

            if is_chemical:
                print(f'Vehicle {v}, Sensor: Chemical')
            elif is_radiation:
                print(f'Vehicle {v}, Sensor: Radiation')
            else:
                print(f'Vehicle {v}, Sensor: Unknown')

        self.__is_discovering = False

    def __parse_vehicle_id(self, topic):
        vehicle_id, _ = topic.split("/")
        if not vehicle_id.isnumeric():
            print(f"Found Sensor: {_} on {vehicle_id}")
            return
        if vehicle_id not in self.__active_vehicles:
            print(f"MQTT: Discovered new vehicle with ID {vehicle_id}")
            self.__active_vehicles.append(vehicle_id)

    def get_discovered_vehicles(self):
        return self.__active_vehicles

    # extract vehicle_ids from each message. using 'set' for distinct values
    def get_connected_vehicles(self):
        return list(set([x.split("/")[0] for x in list(self.__messages)]))
    
    def set_debug_data(self, vehicle_ids):
        settings = GlobalSettings()
        for id in vehicle_ids:
            self.update(id + "/ATTITUDE", "{\"TimeBootMs\":59943,\"Roll\":0.005322347,\"Pitch\":0.0047024703,\"Yaw\":-2.2250977,\"Rollspeed\":-0.00029757782,\"Pitchspeed\":0.00038582017,\"Yawspeed\":0.00025680568}")
            self.update(id + "/DISTANCE_SENSOR", "{\"TimeBootMs\":62910,\"MinDistance\":20,\"MaxDistance\":790,\"CurrentDistance\":26,\"Type\":0,\"Id\":0,\"Orientation\":25,\"Covariance\":0,\"HorizontalFov\":0,\"VerticalFov\":0,\"Quaternion\":[0,0,0,0]}")
            self.update(id + "/ALTITUDE", "{\"TimeUsec\":62999641,\"AltitudeMonotonic\":544.701,\"AltitudeAmsl\":544.701,\"AltitudeLocal\":-0.8937462,\"AltitudeRelative\":-0.8937462,\"AltitudeTerrain\":-1.0937462,\"BottomClearance\":0.2}")
            self.update(id + "/GLOBAL_POSITION_INT", "{\"Lat\": 468564855, \"Lon\": 95173751}")
            self.update(id + "/BATTERY_STATUS", "{\"CurrentConsumed\":36,\"EnergyConsumed\":-1,\"Temperature\":3100,\"Voltages\":[3335,3345,3341,3342,3344,3344,65535,65535,65535,65535],\"CurrentBattery\":37,\"Id\":1,\"BatteryFunction\":1,\"Type\":1,\"BatteryRemaining\":32,\"TimeRemaining\":361,\"ChargeState\":2}")
            self.update(id + "/WIND_COV", "{\"WindX\":-2.508257,\"WindY\":1.5850776}")
            
            if settings.get_chemical_vehicle_id() == id:
                self.update(id + "/CHEMICAL_DETECTOR_DATA", "{\"Concentration\":10,\"Dose\":1,\"Bars\":1}")
            if settings.get_radiation_vehicle_id() == id:
                self.update(id + "/RADIATION_DETECTOR_DATA", "{\"Rates0\":278.4,\"Rates1\":0,\"Counts0\":13,\"Counts1\":0}")

    def connect(self):
        if self.__is_connected:
            return True
        try:
            self.client.connect(self.__ip, self.__port, 60)
            self.client.loop_start()
            #self.__discover_devices()
            self.__is_connected = True
            self.__is_running = True
            self.__messages = {}
            return True
        except:
            return False

    def connect_and_discover_devices(self):
        if not self.__is_connected:
            self.client.connect(self.__ip, self.__port, 60)
            self.client.loop_start()
            self.__discover_devices()
        self.stop()
        return self.get_discovered_vehicles()

    def update(self, topic: str, value: str) -> None:
        try:
            self.__messages[topic] = json.loads(value)
        except:
            print(f"JSON Parse error: {value}")
            pass

    def get_message(self, message_name: str, vehicle_id):
        return self.__messages.get(f"{vehicle_id}/{message_name}")

    def __on_connect(self, client: mqtt.Client, userdata, flags, rc):
        client.subscribe([
            ("+/ATTITUDE", 0),
            ("+/ALTITUDE", 0),
            ("+/GLOBAL_POSITION_INT", 0),
            ("+/GPS_RAW_INT", 0),
            ("+/DISTANCE_SENSOR", 0),
            ("+/WIND_COV", 0),
            ("+/BATTERY_STATUS", 0),
            ("+/CHEMICAL_DETECTOR_DATA", 0),
            ("+/RADIATION_DETECTOR_DATA", 0),
            ])

    def __on_message(self, client, userdata, msg: MQTTMessage):
        try:
            if self.__is_discovering:
                self.__parse_vehicle_id(msg.topic)
                return
            self.update(msg.topic, msg.payload.decode())
        except UnicodeEncodeError:
            print("Error: could not decode MQTT message 'msg.payload'")
            pass