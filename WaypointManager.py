
import math
from pyproj import Transformer

class WaypointManager(object):

    def __init__(self) -> None:
        self.__waypoints_EPSG4326_lat_lon = dict()
        self.__waypoints_EPSG3857_x_y = dict()
        self.__webmercator_to_lon_lat = Transformer.from_crs("EPSG:3857", "EPSG:4326", always_xy=True)

    def get_waypoints_lat_lon(self, vehicle_id):
        return self.__waypoints_EPSG4326_lat_lon.get(vehicle_id)
    
    def get_waypoints_x_y(self, vehicle_id):
        return self.__waypoints_EPSG3857_x_y.get(vehicle_id)

    def build_waypoints(self, vehicle_id, X, Y):
        if len(X) != 4 or len(Y) != 4:
            print("Warn: Can't generate waypoints. Need polygon with exactly four sides.")
            return False
        
        waypoints_x_y = []
        waypoints_lat_lon = []
        
        x1 = X[0]
        y1 = Y[0]

        x2 = X[1]
        y2 = Y[1]
        
        x3 = X[2]
        y3 = Y[2]

        x4 = X[3]
        y4 = Y[3]

        distance_1_2 = abs(math.sqrt((x2 - x1)**2 + (y2-y1)**2))
        distance_3_4 = abs(math.sqrt((x3 - x4)**2 + (y3-y4)**2))
        
        step_size_in_meters = 12
        step_size = step_size_in_meters * 1.5
        number_of_lines = math.ceil((distance_1_2 if distance_1_2 > distance_3_4 else distance_3_4) / step_size)
        
        number_of_lines = number_of_lines + 1 if number_of_lines % 2 == 0 else number_of_lines

        x_1_offset = (x2 - x1) / number_of_lines
        y_1_offset = (y2 - y1) / number_of_lines

        x_2_offset = (x3 - x4) / number_of_lines
        y_2_offset = (y3 - y4) / number_of_lines

        start_waypoint = (x1, y1)
        end_waypoint = (x2,y2)
        waypoints_x_y.append(start_waypoint)

        for i in range(number_of_lines):
            
            if i % 2 == 0:
                x = x4 + x_2_offset * i
                y = y4 + y_2_offset * i
                waypoints_x_y.append((x,y))

                x = x4 + x_2_offset * (i + 1)
                y = y4 + y_2_offset * (i + 1)
                waypoints_x_y.append((x,y))
            else:
                x = x1 + x_1_offset * i
                y = y1 + y_1_offset * i
                waypoints_x_y.append((x,y))

                x = x1 + x_1_offset * (i + 1)
                y = y1 + y_1_offset * (i + 1)
                waypoints_x_y.append((x,y))

        waypoints_x_y.append(end_waypoint)

        for (x, y) in waypoints_x_y:
            lon, lat = self.__webmercator_to_lon_lat.transform(x,y)
            waypoints_lat_lon.append((lat, lon))
        
        self.__waypoints_EPSG3857_x_y[vehicle_id] = waypoints_x_y
        self.__waypoints_EPSG4326_lat_lon[vehicle_id] = waypoints_lat_lon