class SessionSettings():
   
    def __init__(self):
        self.__is_mission_running = False
        self.__is_readonly = True
        self.__is_main = False
        self.__is_live = True
        self.__is_recording = False

    def get_is_live(self):
        return self.__is_live

    def set_is_live(self, value: bool):
        self.__is_live = value

    def get_is_mission_running(self):
        return self.__is_mission_running

    def set_is_mission_running(self, value: bool):
        self.__is_mission_running = value

    def get_is_readonly(self):
        return self.__is_readonly and not self.__is_main
    
    def set_is_readonly(self, is_readonly: bool):
        self.__is_readonly = is_readonly

    def set_is_main(self, value: bool):
        self.__is_main = value

    def get_is_main(self):
        return self.__is_main
    
    def get_is_recording(self):
        return self.__is_recording
    
    def set_is_recording(self, value: bool):
        self.__is_recording = value